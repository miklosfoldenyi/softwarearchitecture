package hu.bme.timesheet.wsdl;

public class ITimesheetManagerInterfaceProxy implements hu.bme.timesheet.wsdl.ITimesheetManagerInterface {
  private String _endpoint = null;
  private hu.bme.timesheet.wsdl.ITimesheetManagerInterface iTimesheetManagerInterface = null;
  
  public ITimesheetManagerInterfaceProxy() {
    _initITimesheetManagerInterfaceProxy();
  }
  
  public ITimesheetManagerInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initITimesheetManagerInterfaceProxy();
  }
  
  private void _initITimesheetManagerInterfaceProxy() {
    try {
      iTimesheetManagerInterface = (new hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator()).getTimesheetManagerPort();
      if (iTimesheetManagerInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iTimesheetManagerInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iTimesheetManagerInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iTimesheetManagerInterface != null)
      ((javax.xml.rpc.Stub)iTimesheetManagerInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public hu.bme.timesheet.wsdl.ITimesheetManagerInterface getITimesheetManagerInterface() {
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface;
  }
  
  public boolean revokeManageRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.revokeManageRole(session, user);
  }
  
  public boolean logout(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.logout(session);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetUserSession login(hu.bme.timesheet.wsdl.TimesheetUser user, java.lang.String password) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.login(user, password);
  }
  
  public boolean addUserToProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.addUserToProject(session, project, user);
  }
  
  public boolean deleteProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.deleteProject(session, project);
  }
  
  public boolean createUser(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user, java.lang.String password) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.createUser(session, user, password);
  }
  
  public boolean removeUserFromProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.removeUserFromProject(session, project, user);
  }
  
  public boolean deleteUser(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.deleteUser(session, user);
  }
  
  public boolean grantManageRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.grantManageRole(session, user);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetProject[] listAllProjects(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.listAllProjects(session);
  }
  
  public boolean grantClientRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.grantClientRole(session, user);
  }
  
  public boolean revokeClientRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.revokeClientRole(session, user);
  }
  
  public boolean createProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.createProject(session, project);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetUser[] listAllUsers(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetManagerInterface == null)
      _initITimesheetManagerInterfaceProxy();
    return iTimesheetManagerInterface.listAllUsers(session);
  }
  
  
}