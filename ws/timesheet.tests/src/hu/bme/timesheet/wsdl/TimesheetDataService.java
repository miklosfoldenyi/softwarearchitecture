/**
 * TimesheetDataService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public interface TimesheetDataService extends javax.xml.rpc.Service {
    public java.lang.String getTimesheetDataPortAddress();

    public hu.bme.timesheet.wsdl.ITimesheetDataInterface getTimesheetDataPort() throws javax.xml.rpc.ServiceException;

    public hu.bme.timesheet.wsdl.ITimesheetDataInterface getTimesheetDataPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
