/**
 * TimesheetManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetManagerServiceLocator extends org.apache.axis.client.Service implements hu.bme.timesheet.wsdl.TimesheetManagerService {

    public TimesheetManagerServiceLocator() {
    }


    public TimesheetManagerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TimesheetManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TimesheetManagerPort
    private java.lang.String TimesheetManagerPort_address = "http://localhost:8080/timesheet.core/webservices/TimesheetManagerInterfaceImpl";

    public java.lang.String getTimesheetManagerPortAddress() {
        return TimesheetManagerPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TimesheetManagerPortWSDDServiceName = "TimesheetManagerPort";

    public java.lang.String getTimesheetManagerPortWSDDServiceName() {
        return TimesheetManagerPortWSDDServiceName;
    }

    public void setTimesheetManagerPortWSDDServiceName(java.lang.String name) {
        TimesheetManagerPortWSDDServiceName = name;
    }

    public hu.bme.timesheet.wsdl.ITimesheetManagerInterface getTimesheetManagerPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TimesheetManagerPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTimesheetManagerPort(endpoint);
    }

    public hu.bme.timesheet.wsdl.ITimesheetManagerInterface getTimesheetManagerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            hu.bme.timesheet.wsdl.TimesheetManagerServiceSoapBindingStub _stub = new hu.bme.timesheet.wsdl.TimesheetManagerServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getTimesheetManagerPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTimesheetManagerPortEndpointAddress(java.lang.String address) {
        TimesheetManagerPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (hu.bme.timesheet.wsdl.ITimesheetManagerInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                hu.bme.timesheet.wsdl.TimesheetManagerServiceSoapBindingStub _stub = new hu.bme.timesheet.wsdl.TimesheetManagerServiceSoapBindingStub(new java.net.URL(TimesheetManagerPort_address), this);
                _stub.setPortName(getTimesheetManagerPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TimesheetManagerPort".equals(inputPortName)) {
            return getTimesheetManagerPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "TimesheetManagerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "TimesheetManagerPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TimesheetManagerPort".equals(portName)) {
            setTimesheetManagerPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
