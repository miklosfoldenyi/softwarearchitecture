/**
 * TimesheetProject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetProject  implements java.io.Serializable {
    private hu.bme.timesheet.wsdl.TimesheetWorkEntry[] associatedEntries;

    private hu.bme.timesheet.wsdl.TimesheetUser[] associatedWorkers;

    private long id;

    private java.lang.String name;

    public TimesheetProject() {
    }

    public TimesheetProject(
           hu.bme.timesheet.wsdl.TimesheetWorkEntry[] associatedEntries,
           hu.bme.timesheet.wsdl.TimesheetUser[] associatedWorkers,
           long id,
           java.lang.String name) {
           this.associatedEntries = associatedEntries;
           this.associatedWorkers = associatedWorkers;
           this.id = id;
           this.name = name;
    }


    /**
     * Gets the associatedEntries value for this TimesheetProject.
     * 
     * @return associatedEntries
     */
    public hu.bme.timesheet.wsdl.TimesheetWorkEntry[] getAssociatedEntries() {
        return associatedEntries;
    }


    /**
     * Sets the associatedEntries value for this TimesheetProject.
     * 
     * @param associatedEntries
     */
    public void setAssociatedEntries(hu.bme.timesheet.wsdl.TimesheetWorkEntry[] associatedEntries) {
        this.associatedEntries = associatedEntries;
    }

    public hu.bme.timesheet.wsdl.TimesheetWorkEntry getAssociatedEntries(int i) {
        return this.associatedEntries[i];
    }

    public void setAssociatedEntries(int i, hu.bme.timesheet.wsdl.TimesheetWorkEntry _value) {
        this.associatedEntries[i] = _value;
    }


    /**
     * Gets the associatedWorkers value for this TimesheetProject.
     * 
     * @return associatedWorkers
     */
    public hu.bme.timesheet.wsdl.TimesheetUser[] getAssociatedWorkers() {
        return associatedWorkers;
    }


    /**
     * Sets the associatedWorkers value for this TimesheetProject.
     * 
     * @param associatedWorkers
     */
    public void setAssociatedWorkers(hu.bme.timesheet.wsdl.TimesheetUser[] associatedWorkers) {
        this.associatedWorkers = associatedWorkers;
    }

    public hu.bme.timesheet.wsdl.TimesheetUser getAssociatedWorkers(int i) {
        return this.associatedWorkers[i];
    }

    public void setAssociatedWorkers(int i, hu.bme.timesheet.wsdl.TimesheetUser _value) {
        this.associatedWorkers[i] = _value;
    }


    /**
     * Gets the id value for this TimesheetProject.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this TimesheetProject.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the name value for this TimesheetProject.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this TimesheetProject.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TimesheetProject)) return false;
        TimesheetProject other = (TimesheetProject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.associatedEntries==null && other.getAssociatedEntries()==null) || 
             (this.associatedEntries!=null &&
              java.util.Arrays.equals(this.associatedEntries, other.getAssociatedEntries()))) &&
            ((this.associatedWorkers==null && other.getAssociatedWorkers()==null) || 
             (this.associatedWorkers!=null &&
              java.util.Arrays.equals(this.associatedWorkers, other.getAssociatedWorkers()))) &&
            this.id == other.getId() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssociatedEntries() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssociatedEntries());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssociatedEntries(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAssociatedWorkers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssociatedWorkers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssociatedWorkers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Long(getId()).hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TimesheetProject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetProject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associatedEntries");
        elemField.setXmlName(new javax.xml.namespace.QName("", "associatedEntries"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetWorkEntry"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associatedWorkers");
        elemField.setXmlName(new javax.xml.namespace.QName("", "associatedWorkers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUser"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
