/**
 * TimesheetUserSession.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetUserSession  implements java.io.Serializable {
    private java.util.Calendar endTime;

    private long id;

    private java.lang.String note;

    private java.lang.String sessionHash;

    private java.util.Calendar startTime;

    private hu.bme.timesheet.wsdl.TimesheetSessionType type;

    private hu.bme.timesheet.wsdl.TimesheetUser user;

    public TimesheetUserSession() {
    }

    public TimesheetUserSession(
           java.util.Calendar endTime,
           long id,
           java.lang.String note,
           java.lang.String sessionHash,
           java.util.Calendar startTime,
           hu.bme.timesheet.wsdl.TimesheetSessionType type,
           hu.bme.timesheet.wsdl.TimesheetUser user) {
           this.endTime = endTime;
           this.id = id;
           this.note = note;
           this.sessionHash = sessionHash;
           this.startTime = startTime;
           this.type = type;
           this.user = user;
    }


    /**
     * Gets the endTime value for this TimesheetUserSession.
     * 
     * @return endTime
     */
    public java.util.Calendar getEndTime() {
        return endTime;
    }


    /**
     * Sets the endTime value for this TimesheetUserSession.
     * 
     * @param endTime
     */
    public void setEndTime(java.util.Calendar endTime) {
        this.endTime = endTime;
    }


    /**
     * Gets the id value for this TimesheetUserSession.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this TimesheetUserSession.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the note value for this TimesheetUserSession.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this TimesheetUserSession.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the sessionHash value for this TimesheetUserSession.
     * 
     * @return sessionHash
     */
    public java.lang.String getSessionHash() {
        return sessionHash;
    }


    /**
     * Sets the sessionHash value for this TimesheetUserSession.
     * 
     * @param sessionHash
     */
    public void setSessionHash(java.lang.String sessionHash) {
        this.sessionHash = sessionHash;
    }


    /**
     * Gets the startTime value for this TimesheetUserSession.
     * 
     * @return startTime
     */
    public java.util.Calendar getStartTime() {
        return startTime;
    }


    /**
     * Sets the startTime value for this TimesheetUserSession.
     * 
     * @param startTime
     */
    public void setStartTime(java.util.Calendar startTime) {
        this.startTime = startTime;
    }


    /**
     * Gets the type value for this TimesheetUserSession.
     * 
     * @return type
     */
    public hu.bme.timesheet.wsdl.TimesheetSessionType getType() {
        return type;
    }


    /**
     * Sets the type value for this TimesheetUserSession.
     * 
     * @param type
     */
    public void setType(hu.bme.timesheet.wsdl.TimesheetSessionType type) {
        this.type = type;
    }


    /**
     * Gets the user value for this TimesheetUserSession.
     * 
     * @return user
     */
    public hu.bme.timesheet.wsdl.TimesheetUser getUser() {
        return user;
    }


    /**
     * Sets the user value for this TimesheetUserSession.
     * 
     * @param user
     */
    public void setUser(hu.bme.timesheet.wsdl.TimesheetUser user) {
        this.user = user;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TimesheetUserSession)) return false;
        TimesheetUserSession other = (TimesheetUserSession) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.endTime==null && other.getEndTime()==null) || 
             (this.endTime!=null &&
              this.endTime.equals(other.getEndTime()))) &&
            this.id == other.getId() &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.sessionHash==null && other.getSessionHash()==null) || 
             (this.sessionHash!=null &&
              this.sessionHash.equals(other.getSessionHash()))) &&
            ((this.startTime==null && other.getStartTime()==null) || 
             (this.startTime!=null &&
              this.startTime.equals(other.getStartTime()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEndTime() != null) {
            _hashCode += getEndTime().hashCode();
        }
        _hashCode += new Long(getId()).hashCode();
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getSessionHash() != null) {
            _hashCode += getSessionHash().hashCode();
        }
        if (getStartTime() != null) {
            _hashCode += getStartTime().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TimesheetUserSession.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUserSession"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionHash");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetSessionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUser"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
