/**
 * ITimesheetClientInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public interface ITimesheetClientInterface extends java.rmi.Remote {
    public boolean logout(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetUserSession login(hu.bme.timesheet.wsdl.TimesheetUser user, java.lang.String password) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetWorkEntry[] getAssociatedEntries(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean addUserToWork(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetWorkEntry workEntry, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetProject[] getAssociatedProjects(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean addWork(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetWorkEntry workEntry) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetUser[] getUsersForProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
}
