/**
 * ITimesheetManagerInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public interface ITimesheetManagerInterface extends java.rmi.Remote {
    public boolean revokeManageRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean logout(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetUserSession login(hu.bme.timesheet.wsdl.TimesheetUser user, java.lang.String password) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean addUserToProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean deleteProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean createUser(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user, java.lang.String password) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean removeUserFromProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean deleteUser(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean grantManageRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetProject[] listAllProjects(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean grantClientRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean revokeClientRole(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetUser user) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public boolean createProject(hu.bme.timesheet.wsdl.TimesheetUserSession session, hu.bme.timesheet.wsdl.TimesheetProject project) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetUser[] listAllUsers(hu.bme.timesheet.wsdl.TimesheetUserSession session) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
}
