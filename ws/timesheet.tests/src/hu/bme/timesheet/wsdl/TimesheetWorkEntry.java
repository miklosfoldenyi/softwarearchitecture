/**
 * TimesheetWorkEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetWorkEntry  implements java.io.Serializable {
    private hu.bme.timesheet.wsdl.TimesheetProject associatedProject;

    private hu.bme.timesheet.wsdl.TimesheetUser[] attendees;

    private java.util.Calendar end;

    private long id;

    private java.util.Calendar start;

    public TimesheetWorkEntry() {
    }

    public TimesheetWorkEntry(
           hu.bme.timesheet.wsdl.TimesheetProject associatedProject,
           hu.bme.timesheet.wsdl.TimesheetUser[] attendees,
           java.util.Calendar end,
           long id,
           java.util.Calendar start) {
           this.associatedProject = associatedProject;
           this.attendees = attendees;
           this.end = end;
           this.id = id;
           this.start = start;
    }


    /**
     * Gets the associatedProject value for this TimesheetWorkEntry.
     * 
     * @return associatedProject
     */
    public hu.bme.timesheet.wsdl.TimesheetProject getAssociatedProject() {
        return associatedProject;
    }


    /**
     * Sets the associatedProject value for this TimesheetWorkEntry.
     * 
     * @param associatedProject
     */
    public void setAssociatedProject(hu.bme.timesheet.wsdl.TimesheetProject associatedProject) {
        this.associatedProject = associatedProject;
    }


    /**
     * Gets the attendees value for this TimesheetWorkEntry.
     * 
     * @return attendees
     */
    public hu.bme.timesheet.wsdl.TimesheetUser[] getAttendees() {
        return attendees;
    }


    /**
     * Sets the attendees value for this TimesheetWorkEntry.
     * 
     * @param attendees
     */
    public void setAttendees(hu.bme.timesheet.wsdl.TimesheetUser[] attendees) {
        this.attendees = attendees;
    }

    public hu.bme.timesheet.wsdl.TimesheetUser getAttendees(int i) {
        return this.attendees[i];
    }

    public void setAttendees(int i, hu.bme.timesheet.wsdl.TimesheetUser _value) {
        this.attendees[i] = _value;
    }


    /**
     * Gets the end value for this TimesheetWorkEntry.
     * 
     * @return end
     */
    public java.util.Calendar getEnd() {
        return end;
    }


    /**
     * Sets the end value for this TimesheetWorkEntry.
     * 
     * @param end
     */
    public void setEnd(java.util.Calendar end) {
        this.end = end;
    }


    /**
     * Gets the id value for this TimesheetWorkEntry.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this TimesheetWorkEntry.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the start value for this TimesheetWorkEntry.
     * 
     * @return start
     */
    public java.util.Calendar getStart() {
        return start;
    }


    /**
     * Sets the start value for this TimesheetWorkEntry.
     * 
     * @param start
     */
    public void setStart(java.util.Calendar start) {
        this.start = start;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TimesheetWorkEntry)) return false;
        TimesheetWorkEntry other = (TimesheetWorkEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.associatedProject==null && other.getAssociatedProject()==null) || 
             (this.associatedProject!=null &&
              this.associatedProject.equals(other.getAssociatedProject()))) &&
            ((this.attendees==null && other.getAttendees()==null) || 
             (this.attendees!=null &&
              java.util.Arrays.equals(this.attendees, other.getAttendees()))) &&
            ((this.end==null && other.getEnd()==null) || 
             (this.end!=null &&
              this.end.equals(other.getEnd()))) &&
            this.id == other.getId() &&
            ((this.start==null && other.getStart()==null) || 
             (this.start!=null &&
              this.start.equals(other.getStart())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssociatedProject() != null) {
            _hashCode += getAssociatedProject().hashCode();
        }
        if (getAttendees() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttendees());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttendees(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEnd() != null) {
            _hashCode += getEnd().hashCode();
        }
        _hashCode += new Long(getId()).hashCode();
        if (getStart() != null) {
            _hashCode += getStart().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TimesheetWorkEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetWorkEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associatedProject");
        elemField.setXmlName(new javax.xml.namespace.QName("", "associatedProject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetProject"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attendees");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attendees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUser"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
