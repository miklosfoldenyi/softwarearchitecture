/**
 * TimesheetClientServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetClientServiceLocator extends org.apache.axis.client.Service implements hu.bme.timesheet.wsdl.TimesheetClientService {

    public TimesheetClientServiceLocator() {
    }


    public TimesheetClientServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TimesheetClientServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TimesheetClientPort
    private java.lang.String TimesheetClientPort_address = "http://localhost:8080/timesheet.core/webservices/TimesheetClientInterfaceImpl";

    public java.lang.String getTimesheetClientPortAddress() {
        return TimesheetClientPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TimesheetClientPortWSDDServiceName = "TimesheetClientPort";

    public java.lang.String getTimesheetClientPortWSDDServiceName() {
        return TimesheetClientPortWSDDServiceName;
    }

    public void setTimesheetClientPortWSDDServiceName(java.lang.String name) {
        TimesheetClientPortWSDDServiceName = name;
    }

    public hu.bme.timesheet.wsdl.ITimesheetClientInterface getTimesheetClientPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TimesheetClientPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTimesheetClientPort(endpoint);
    }

    public hu.bme.timesheet.wsdl.ITimesheetClientInterface getTimesheetClientPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            hu.bme.timesheet.wsdl.TimesheetClientServiceSoapBindingStub _stub = new hu.bme.timesheet.wsdl.TimesheetClientServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getTimesheetClientPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTimesheetClientPortEndpointAddress(java.lang.String address) {
        TimesheetClientPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (hu.bme.timesheet.wsdl.ITimesheetClientInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                hu.bme.timesheet.wsdl.TimesheetClientServiceSoapBindingStub _stub = new hu.bme.timesheet.wsdl.TimesheetClientServiceSoapBindingStub(new java.net.URL(TimesheetClientPort_address), this);
                _stub.setPortName(getTimesheetClientPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TimesheetClientPort".equals(inputPortName)) {
            return getTimesheetClientPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "TimesheetClientService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "TimesheetClientPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TimesheetClientPort".equals(portName)) {
            setTimesheetClientPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
