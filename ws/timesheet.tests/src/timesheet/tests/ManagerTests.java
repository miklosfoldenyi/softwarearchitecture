package timesheet.tests;

import static org.junit.Assert.assertEquals;
import hu.bme.timesheet.wsdl.ITimesheetClientInterface;
import hu.bme.timesheet.wsdl.ITimesheetDataInterface;
import hu.bme.timesheet.wsdl.ITimesheetManagerInterface;
import hu.bme.timesheet.wsdl.TimesheetClientServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetDataServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetFault;
import hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetProject;
import hu.bme.timesheet.wsdl.TimesheetUser;
import hu.bme.timesheet.wsdl.TimesheetUserSession;
import hu.bme.timesheet.wsdl.TimesheetWorkEntry;

import java.rmi.RemoteException;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import org.junit.Before;
import org.junit.Test;

public class ManagerTests {

	@Before
	public void init() throws ServiceException {
		timesheetManagerPort = new TimesheetManagerServiceLocator().getTimesheetManagerPort();
		timesheetClientPort = new TimesheetClientServiceLocator().getTimesheetClientPort();
		timesheetDataPort = new TimesheetDataServiceLocator().getTimesheetDataPort();
	}

	private ITimesheetClientInterface timesheetClientPort;
	private ITimesheetManagerInterface timesheetManagerPort;
	private ITimesheetDataInterface timesheetDataPort;

	public static final String ADMIN_USERNAME = "admin";
	public static final String ADMIN_PASSWORD = "admin";

	public static final String TEST_USER1_NAME = "F�ld�nyi Mikl�s" + System.currentTimeMillis();
	public static final String TEST_USER1_USERNAME = "miki" + System.currentTimeMillis();
	public static final String TEST_USER1_PASSWORD = "abcd1234" + System.currentTimeMillis();

	public static final String TEST_USER2_NAME = "Tam�s �va" + System.currentTimeMillis();
	public static final String TEST_USER2_USERNAME = "evi" + System.currentTimeMillis();
	public static final String TEST_USER2_PASSWORD = "abcd1234" + System.currentTimeMillis();

	public static final String TEST_PROJECT1_NAME = "SzAr Homework";

	@Test
	public void testCase_CreateUser_GrantRole_ClientLogin_ClientLogout_RevokeRole_DeleteUser() throws TimesheetFault,
			RemoteException {
		// //////////////////////////
		// Entity setup
		// //////////////////////////

		// admin user
		TimesheetUser admin = new TimesheetUser();
		admin.setUsername(ADMIN_USERNAME);

		// create user
		TimesheetUser user = new TimesheetUser();
		user.setName(TEST_USER1_NAME);
		user.setUsername(TEST_USER1_USERNAME);

		// //////////////////////////
		// Entity setup end
		// //////////////////////////

		// admin login
		TimesheetUserSession adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// create user
		timesheetManagerPort.createUser(adminSession, user, TEST_USER1_PASSWORD);
		user = timesheetDataPort.getUserByName(adminSession, TEST_USER1_USERNAME);

		timesheetManagerPort.grantClientRole(adminSession, user);

		// admin logout
		timesheetManagerPort.logout(adminSession);

		// user login
		TimesheetUserSession userSession = timesheetClientPort.login(user, TEST_USER1_PASSWORD);

		// user logout
		timesheetClientPort.logout(userSession);

		// admin relogin
		adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// delete login user
		timesheetManagerPort.deleteUser(adminSession, user);

		// admin logout
		timesheetManagerPort.logout(adminSession);
	}

	@Test
	public void testCase_CreateProject_DeleteProject() throws TimesheetFault, RemoteException {
		// //////////////////////////
		// Entity setup
		// //////////////////////////

		// admin user
		TimesheetUser admin = new TimesheetUser();
		admin.setUsername(ADMIN_USERNAME);

		// //////////////////////////
		// Entity setup end
		// //////////////////////////

		TimesheetUserSession session = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		TimesheetProject project = new TimesheetProject();
		project.setName(TEST_PROJECT1_NAME);

		timesheetManagerPort.createProject(session, project);
		timesheetManagerPort.deleteProject(session, project);

		timesheetManagerPort.logout(session);
	}

	@Test
	public void testCase_CreateProjectAndUser_AddUserToProject_RemoveUserFromProject_DeleteUserAndProject()
			throws TimesheetFault, RemoteException {
		// //////////////////////////
		// Entity setup
		// //////////////////////////

		// admin user
		TimesheetUser admin = new TimesheetUser();
		admin.setUsername(ADMIN_USERNAME);

		// create user
		TimesheetUser user = new TimesheetUser();
		user.setName(TEST_USER1_NAME);
		user.setUsername(TEST_USER1_USERNAME);

		// project
		TimesheetProject project = new TimesheetProject();
		project.setName(TEST_PROJECT1_NAME);

		// //////////////////////////
		// Entity setup end
		// //////////////////////////

		// admin login
		TimesheetUserSession adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// create user
		timesheetManagerPort.createUser(adminSession, user, TEST_USER1_PASSWORD);
		user = timesheetDataPort.getUserByName(adminSession, user.getUsername());
		timesheetManagerPort.grantClientRole(adminSession, user);

		// create project
		timesheetManagerPort.createProject(adminSession, project);
		project = timesheetDataPort.getProjectByName(adminSession, TEST_PROJECT1_NAME);

		// associate user and project
		timesheetManagerPort.addUserToProject(adminSession, project, user);

		// remove association
		timesheetManagerPort.removeUserFromProject(adminSession, project, user);

		// remove project
		timesheetManagerPort.deleteProject(adminSession, project);

		// admin logout
		timesheetManagerPort.logout(adminSession);

		// user login
		TimesheetUserSession userSession = timesheetClientPort.login(user, TEST_USER1_PASSWORD);

		// user logout
		timesheetClientPort.logout(userSession);

		// admin relogin
		adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// delete login user
		timesheetManagerPort.deleteUser(adminSession, user);

		// admin logout
		timesheetManagerPort.logout(adminSession);
	}

	@Test
	public void testCase_CreateProjectAndUser_AddUserToProject_AddWork_AddCoWorker_RemoveUserFromProject_DeleteUserAndProject()
			throws TimesheetFault, RemoteException {
		// //////////////////////////
		// Entity setup
		// //////////////////////////

		// admin user
		TimesheetUser admin = new TimesheetUser();
		admin.setUsername(ADMIN_USERNAME);

		// create user
		TimesheetUser user1 = new TimesheetUser();
		user1.setName(TEST_USER1_NAME);
		user1.setUsername(TEST_USER1_USERNAME);


		TimesheetUser user2 = new TimesheetUser();
		user2.setName(TEST_USER2_NAME);
		user2.setUsername(TEST_USER2_USERNAME);

		// project
		TimesheetProject project = new TimesheetProject();
		project.setName(TEST_PROJECT1_NAME);

		// //////////////////////////
		// Entity setup end
		// //////////////////////////

		// admin login
		TimesheetUserSession adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// create users
		timesheetManagerPort.createUser(adminSession, user1, TEST_USER1_PASSWORD);
		timesheetManagerPort.createUser(adminSession, user2, TEST_USER2_PASSWORD);
		user1 = timesheetDataPort.getUserByName(adminSession, user1.getUsername());
		user2 = timesheetDataPort.getUserByName(adminSession, user2.getUsername());
		timesheetManagerPort.grantClientRole(adminSession, user1);
		timesheetManagerPort.grantClientRole(adminSession, user2);

		// create project
		timesheetManagerPort.createProject(adminSession, project);
		project = timesheetDataPort.getProjectByName(adminSession, TEST_PROJECT1_NAME);

		// associate user and project
		timesheetManagerPort.addUserToProject(adminSession, project, user1);
		timesheetManagerPort.addUserToProject(adminSession, project, user2);

		
		// user1 logs in
		TimesheetUserSession clientSession = timesheetClientPort.login(user1, TEST_USER1_PASSWORD);
		
		TimesheetProject[] associatedProjects = timesheetClientPort.getAssociatedProjects(clientSession);
		
		TimesheetProject pr = associatedProjects[0];
		
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		end.add(Calendar.HOUR, 2);

		TimesheetWorkEntry entry = new TimesheetWorkEntry();
		entry.setAssociatedProject(pr);		
		entry.setStart(start);
		entry.setEnd(end);
		entry.setAttendees(new TimesheetUser[1]);
		entry.getAttendees()[0] = user1;
				
		timesheetClientPort.addWork(clientSession, entry);
		
		TimesheetWorkEntry[] associatedEntries = timesheetClientPort.getAssociatedEntries(clientSession);
		entry = associatedEntries[0];
		
		TimesheetUser[] usersForProject = timesheetClientPort.getUsersForProject(clientSession, pr);
		TimesheetUser buddy = null;
		for (TimesheetUser usr : usersForProject) {
			if (usr.getId() != user1.getId()) {
				buddy = usr;
			}
		}
		
		timesheetClientPort.addUserToWork(clientSession, entry, buddy);		
		
		// user1 logs out
		timesheetClientPort.logout(clientSession);
		
		// remove association
		timesheetManagerPort.removeUserFromProject(adminSession, project, user1);
		timesheetManagerPort.removeUserFromProject(adminSession, project, user2);

		// remove project
		timesheetManagerPort.deleteProject(adminSession, project);

		// admin logout
		timesheetManagerPort.logout(adminSession);

		// user login
		TimesheetUserSession userSession = timesheetClientPort.login(user1, TEST_USER1_PASSWORD);

		// user logout
		timesheetClientPort.logout(userSession);

		// admin relogin
		adminSession = timesheetManagerPort.login(admin, ADMIN_PASSWORD);

		// delete login user
		timesheetManagerPort.deleteUser(adminSession, user1);

		// admin logout
		timesheetManagerPort.logout(adminSession);
	}
}
