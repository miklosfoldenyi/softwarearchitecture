import hu.bme.timesheet.wsdl.ITimesheetDataInterface;
import hu.bme.timesheet.wsdl.ITimesheetManagerInterface;
import hu.bme.timesheet.wsdl.TimesheetDataServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetProject;
import hu.bme.timesheet.wsdl.TimesheetUser;
import hu.bme.timesheet.wsdl.TimesheetUserSession;
import hu.bme.timesheet.wsdl.TimesheetWorkEntry;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

/**
 * Servlet implementation class ReportServlet
 */
@WebServlet("/report")
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	private ITimesheetManagerInterface manager;

	private ITimesheetDataInterface data;

	private TimesheetUserSession sessionHash;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		manager = (ITimesheetManagerInterface) request.getSession().getAttribute("manager");
		if (manager == null) {
			try {
				manager = new TimesheetManagerServiceLocator().getTimesheetManagerPort();
				request.getSession().setAttribute("manager", manager);
			} catch (ServiceException e) {
				request.setAttribute("errormessage", e);
				request.getRequestDispatcher("faces/error.jsp").forward(request, response);
				return;
			}
		}
		data = (ITimesheetDataInterface) request.getSession().getAttribute("data");
		if (data == null) {
			try {
				data = new TimesheetDataServiceLocator().getTimesheetDataPort();
				request.getSession().setAttribute("data", data);
			} catch (ServiceException e) {
				request.setAttribute("errormessage", e);
				request.getRequestDispatcher("faces/error.jsp").forward(request, response);
				return;
			}
		}

		sessionHash = (TimesheetUserSession) request.getSession().getAttribute("sessionHash");

		if (sessionHash == null) {
			response.sendRedirect("login");
			return;
		}

		if (doActions(request, response)) {
			return;
		}

		try {
			TimesheetUser[] allUsers = manager.listAllUsers(sessionHash);
			request.setAttribute("ts_users", allUsers);

			TimesheetProject[] allProjects = manager.listAllProjects(sessionHash);
			request.setAttribute("ts_projects", allProjects);

			// Map<TimesheetUser, List<TimesheetProject>> projectMap = new
			// HashMap<>();
			//
			// for (TimesheetUser user : allUsers) {
			// List<TimesheetProject> prjs = new ArrayList<>();
			// projectMap.put(user, prjs);
			// if (user.getAssociatedProjects() == null)
			// continue;
			// for (TimesheetProject userProject : user.getAssociatedProjects())
			// {
			// for (TimesheetProject project : allProjects) {
			// if (userProject.getId() == project.getId()) {
			// prjs.add(project);
			// break;
			// }
			// }
			// }
			// }
			// request.setAttribute("ts_user_project_map", projectMap);

		} catch (RemoteException e) {
			error(request, response, e);
			return;
		}

		request.getRequestDispatcher("faces/reportconfig.jsp").forward(request, response);
	}

	private void error(HttpServletRequest request, HttpServletResponse response, Object msg) throws ServletException,
			IOException {
		request.setAttribute("errormessage", msg);
		request.getRequestDispatcher("faces/error.jsp").forward(request, response);
	}

	private boolean doActions(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		boolean done = false;
		done = doGraph(request, response) ? true : done;
		done = doTable(request, response) ? true : done;
		return done;
	}

	private boolean doGraph(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("generate".equals(request.getParameter("action")) && "graph".equals(request.getParameter("reporttype"))
				&& request.getParameterValues("userid") != null && request.getParameterValues("projectid") != null) {

			List<TimesheetUser> users = new ArrayList<>();

			Map<Long, Long> workMapByUserId = new HashMap<>();
			
			String projectsString = null;

			for (String userid : request.getParameterValues("userid")) {
				TimesheetUser user = data.getUserById(sessionHash, Long.parseLong(userid));
				users.add(user);
				workMapByUserId.put(user.getId(), 0L);
			}

			List<TimesheetProject> projects = new ArrayList<>();

			for (String projectid : request.getParameterValues("projectid")) {
				TimesheetProject project = data.getProjectById(sessionHash, Long.parseLong(projectid));
				projects.add(project);
				if (projectsString == null) {
					projectsString = project.getName();
				} else {
					projectsString += ", "+project.getName();
				}
				
			}

			TimesheetWorkEntry[] entriesForUsersAndProjects = manager.listEntriesForUsersAndProjects(sessionHash,
					users.toArray(new TimesheetUser[users.size()]),
					projects.toArray(new TimesheetProject[projects.size()]));


			if (entriesForUsersAndProjects == null) {
				entriesForUsersAndProjects = new TimesheetWorkEntry[0];
			}

			for (TimesheetWorkEntry entry : entriesForUsersAndProjects) {
				long durationinSeconds = (entry.getEnd().getTimeInMillis() - entry.getStart().getTimeInMillis()) / 1000;
				for (TimesheetUser user : entry.getAttendees()) {

					long workSoFar = workMapByUserId.get(user.getId());
					workMapByUserId.put(user.getId(), workSoFar + durationinSeconds);
				}
			}

			request.setAttribute("ts_users", users);
			request.setAttribute("ts_projects", projects);
			request.setAttribute("ts_work_map", workMapByUserId);
			request.setAttribute("ts_projects_string", projectsString);
			
			request.getRequestDispatcher("faces/report.jsp").forward(request, response);

			return true;
		}

		return false;

	}

	private boolean doTable(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("generate".equals(request.getParameter("action")) && "table".equals(request.getParameter("reporttype"))
				&& request.getParameterValues("userid") != null && request.getParameterValues("projectid") != null) {

			List<TimesheetUser> users = new ArrayList<>();

			Map<Long, Map<Long, Long>> workMapByUserId = new HashMap<>();

			Long total = 0L;

			Map<Long, Long> userTotal = new HashMap<>();
			Map<Long, Long> projectTotal = new HashMap<>();

			for (String userid : request.getParameterValues("userid")) {
				TimesheetUser user = data.getUserById(sessionHash, Long.parseLong(userid));
				users.add(user);
				workMapByUserId.put(user.getId(), new HashMap<Long, Long>());
				userTotal.put(user.getId(), 0L);
			}

			List<TimesheetProject> projects = new ArrayList<>();

			for (String projectid : request.getParameterValues("projectid")) {
				TimesheetProject project = data.getProjectById(sessionHash, Long.parseLong(projectid));
				projectTotal.put(project.getId(), 0L);
				projects.add(project);
				for (Long key : workMapByUserId.keySet()) {
					workMapByUserId.get(key).put(project.getId(), 0L);
				}
			}

			TimesheetWorkEntry[] entriesForUsersAndProjects = manager.listEntriesForUsersAndProjects(sessionHash,
					users.toArray(new TimesheetUser[users.size()]),
					projects.toArray(new TimesheetProject[projects.size()]));

			if (entriesForUsersAndProjects == null) {
				entriesForUsersAndProjects = new TimesheetWorkEntry[0];
			}

			for (TimesheetWorkEntry entry : entriesForUsersAndProjects) {
				long durationInSeconds = (entry.getEnd().getTimeInMillis() - entry.getStart().getTimeInMillis()) / 1000;
				for (TimesheetUser user : entry.getAttendees()) {
					total += durationInSeconds;

					long userTotalSoFar = userTotal.get(user.getId());
					userTotal.put(user.getId(), userTotalSoFar + durationInSeconds);

					long projectTotalSoFar = projectTotal.get(entry.getAssociatedProject().getId());
					projectTotal.put(entry.getAssociatedProject().getId(), projectTotalSoFar + durationInSeconds);

					long workSoFar = workMapByUserId.get(user.getId()).get(entry.getAssociatedProject().getId());
					workMapByUserId.get(user.getId()).put(entry.getAssociatedProject().getId(),
							workSoFar + durationInSeconds);
				}
			}

			request.setAttribute("ts_users", users);
			request.setAttribute("ts_projects", projects);
			request.setAttribute("ts_user_total", userTotal);
			request.setAttribute("ts_project_total", projectTotal);
			request.setAttribute("ts_total", total);
			request.setAttribute("ts_work_map", workMapByUserId);

			request.getRequestDispatcher("faces/report_table.jsp").forward(request, response);

			return true;
		}

		return false;

	}
}
