import hu.bme.timesheet.wsdl.ITimesheetManagerInterface;
import hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetUser;
import hu.bme.timesheet.wsdl.TimesheetUserSession;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ITimesheetManagerInterface manager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		manager = (ITimesheetManagerInterface) request.getSession().getAttribute("manager");
		if (manager == null) {
			try {
				manager = new TimesheetManagerServiceLocator().getTimesheetManagerPort();
				request.getSession().setAttribute("manager", manager);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}

		// extract login data
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if (username != null && password != null) {
			// try login
			TimesheetUser user = new TimesheetUser();
			user.setUsername(username);
			try {
				TimesheetUserSession s = manager.login(user, password);
				if (s != null) {
					request.getSession().setAttribute("sessionHash", s);
					response.sendRedirect("manager");
					return;
				} else {
					request.setAttribute("errormessage", "Login failed");
					request.getRequestDispatcher("faces/error.jsp").forward(request, response);
					return;
				}
			} catch (RemoteException e) {
				request.setAttribute("errormessage", e);
				request.getRequestDispatcher("faces/error.jsp").forward(request, response);
				return;
			}
		}

		request.getRequestDispatcher("faces/login.jsp").forward(request, response);
	}

}
