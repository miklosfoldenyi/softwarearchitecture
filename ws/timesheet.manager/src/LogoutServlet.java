
import hu.bme.timesheet.wsdl.ITimesheetManagerInterface;
import hu.bme.timesheet.wsdl.TimesheetFault;
import hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetUserSession;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogoutServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		ITimesheetManagerInterface manager = (ITimesheetManagerInterface) request.getSession().getAttribute("manager");
		if (manager == null) {
			try {
				manager = new TimesheetManagerServiceLocator().getTimesheetManagerPort();
				request.getSession().setAttribute("manager", manager);
			} catch (ServiceException e) {
				request.setAttribute("errormessage", e.getMessage());
				request.getRequestDispatcher("faces/error.jsp").forward(request, response);
				return;
			}
		}

		TimesheetUserSession sessionHash = (TimesheetUserSession) request.getSession().getAttribute("sessionHash");

		if (sessionHash == null) {
			response.sendRedirect("login");
			return;
		} else {
			request.getSession().removeAttribute("sessionHash");
		}
		try {			
			manager.logout(sessionHash);
			response.sendRedirect("login");
			return;
		} catch (TimesheetFault e) {
			request.setAttribute("errormessage", e.getMessage());
			request.getRequestDispatcher("faces/error.jsp").forward(request, response);
			return;
		}
	}

}
