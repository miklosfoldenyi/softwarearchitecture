package hu.bme.timesheet.wsdl;

public class ITimesheetDataInterfaceProxy implements hu.bme.timesheet.wsdl.ITimesheetDataInterface {
  private String _endpoint = null;
  private hu.bme.timesheet.wsdl.ITimesheetDataInterface iTimesheetDataInterface = null;
  
  public ITimesheetDataInterfaceProxy() {
    _initITimesheetDataInterfaceProxy();
  }
  
  public ITimesheetDataInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initITimesheetDataInterfaceProxy();
  }
  
  private void _initITimesheetDataInterfaceProxy() {
    try {
      iTimesheetDataInterface = (new hu.bme.timesheet.wsdl.TimesheetDataServiceLocator()).getTimesheetDataPort();
      if (iTimesheetDataInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iTimesheetDataInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iTimesheetDataInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iTimesheetDataInterface != null)
      ((javax.xml.rpc.Stub)iTimesheetDataInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public hu.bme.timesheet.wsdl.ITimesheetDataInterface getITimesheetDataInterface() {
    if (iTimesheetDataInterface == null)
      _initITimesheetDataInterfaceProxy();
    return iTimesheetDataInterface;
  }
  
  public hu.bme.timesheet.wsdl.TimesheetUser getUserById(hu.bme.timesheet.wsdl.TimesheetUserSession session, long userId) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetDataInterface == null)
      _initITimesheetDataInterfaceProxy();
    return iTimesheetDataInterface.getUserById(session, userId);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetUser getUserByName(hu.bme.timesheet.wsdl.TimesheetUserSession session, java.lang.String username) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetDataInterface == null)
      _initITimesheetDataInterfaceProxy();
    return iTimesheetDataInterface.getUserByName(session, username);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetProject getProjectById(hu.bme.timesheet.wsdl.TimesheetUserSession session, long projectId) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetDataInterface == null)
      _initITimesheetDataInterfaceProxy();
    return iTimesheetDataInterface.getProjectById(session, projectId);
  }
  
  public hu.bme.timesheet.wsdl.TimesheetProject getProjectByName(hu.bme.timesheet.wsdl.TimesheetUserSession session, java.lang.String projectName) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault{
    if (iTimesheetDataInterface == null)
      _initITimesheetDataInterfaceProxy();
    return iTimesheetDataInterface.getProjectByName(session, projectName);
  }
  
  
}