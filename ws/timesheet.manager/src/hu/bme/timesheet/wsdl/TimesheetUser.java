/**
 * TimesheetUser.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public class TimesheetUser  implements java.io.Serializable {
    private hu.bme.timesheet.wsdl.TimesheetProject[] associatedProjects;

    private hu.bme.timesheet.wsdl.TimesheetWorkEntry[] entries;

    private long id;

    private java.lang.String name;

    private java.lang.String passwordHash;

    private hu.bme.timesheet.wsdl.TimesheetUserRole[] roles;

    private java.lang.String username;

    public TimesheetUser() {
    }

    public TimesheetUser(
           hu.bme.timesheet.wsdl.TimesheetProject[] associatedProjects,
           hu.bme.timesheet.wsdl.TimesheetWorkEntry[] entries,
           long id,
           java.lang.String name,
           java.lang.String passwordHash,
           hu.bme.timesheet.wsdl.TimesheetUserRole[] roles,
           java.lang.String username) {
           this.associatedProjects = associatedProjects;
           this.entries = entries;
           this.id = id;
           this.name = name;
           this.passwordHash = passwordHash;
           this.roles = roles;
           this.username = username;
    }


    /**
     * Gets the associatedProjects value for this TimesheetUser.
     * 
     * @return associatedProjects
     */
    public hu.bme.timesheet.wsdl.TimesheetProject[] getAssociatedProjects() {
        return associatedProjects;
    }


    /**
     * Sets the associatedProjects value for this TimesheetUser.
     * 
     * @param associatedProjects
     */
    public void setAssociatedProjects(hu.bme.timesheet.wsdl.TimesheetProject[] associatedProjects) {
        this.associatedProjects = associatedProjects;
    }

    public hu.bme.timesheet.wsdl.TimesheetProject getAssociatedProjects(int i) {
        return this.associatedProjects[i];
    }

    public void setAssociatedProjects(int i, hu.bme.timesheet.wsdl.TimesheetProject _value) {
        this.associatedProjects[i] = _value;
    }


    /**
     * Gets the entries value for this TimesheetUser.
     * 
     * @return entries
     */
    public hu.bme.timesheet.wsdl.TimesheetWorkEntry[] getEntries() {
        return entries;
    }


    /**
     * Sets the entries value for this TimesheetUser.
     * 
     * @param entries
     */
    public void setEntries(hu.bme.timesheet.wsdl.TimesheetWorkEntry[] entries) {
        this.entries = entries;
    }

    public hu.bme.timesheet.wsdl.TimesheetWorkEntry getEntries(int i) {
        return this.entries[i];
    }

    public void setEntries(int i, hu.bme.timesheet.wsdl.TimesheetWorkEntry _value) {
        this.entries[i] = _value;
    }


    /**
     * Gets the id value for this TimesheetUser.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this TimesheetUser.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the name value for this TimesheetUser.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this TimesheetUser.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the passwordHash value for this TimesheetUser.
     * 
     * @return passwordHash
     */
    public java.lang.String getPasswordHash() {
        return passwordHash;
    }


    /**
     * Sets the passwordHash value for this TimesheetUser.
     * 
     * @param passwordHash
     */
    public void setPasswordHash(java.lang.String passwordHash) {
        this.passwordHash = passwordHash;
    }


    /**
     * Gets the roles value for this TimesheetUser.
     * 
     * @return roles
     */
    public hu.bme.timesheet.wsdl.TimesheetUserRole[] getRoles() {
        return roles;
    }


    /**
     * Sets the roles value for this TimesheetUser.
     * 
     * @param roles
     */
    public void setRoles(hu.bme.timesheet.wsdl.TimesheetUserRole[] roles) {
        this.roles = roles;
    }

    public hu.bme.timesheet.wsdl.TimesheetUserRole getRoles(int i) {
        return this.roles[i];
    }

    public void setRoles(int i, hu.bme.timesheet.wsdl.TimesheetUserRole _value) {
        this.roles[i] = _value;
    }


    /**
     * Gets the username value for this TimesheetUser.
     * 
     * @return username
     */
    public java.lang.String getUsername() {
        return username;
    }


    /**
     * Sets the username value for this TimesheetUser.
     * 
     * @param username
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TimesheetUser)) return false;
        TimesheetUser other = (TimesheetUser) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.associatedProjects==null && other.getAssociatedProjects()==null) || 
             (this.associatedProjects!=null &&
              java.util.Arrays.equals(this.associatedProjects, other.getAssociatedProjects()))) &&
            ((this.entries==null && other.getEntries()==null) || 
             (this.entries!=null &&
              java.util.Arrays.equals(this.entries, other.getEntries()))) &&
            this.id == other.getId() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.passwordHash==null && other.getPasswordHash()==null) || 
             (this.passwordHash!=null &&
              this.passwordHash.equals(other.getPasswordHash()))) &&
            ((this.roles==null && other.getRoles()==null) || 
             (this.roles!=null &&
              java.util.Arrays.equals(this.roles, other.getRoles()))) &&
            ((this.username==null && other.getUsername()==null) || 
             (this.username!=null &&
              this.username.equals(other.getUsername())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssociatedProjects() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssociatedProjects());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssociatedProjects(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEntries() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEntries());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEntries(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Long(getId()).hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getPasswordHash() != null) {
            _hashCode += getPasswordHash().hashCode();
        }
        if (getRoles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRoles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRoles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUsername() != null) {
            _hashCode += getUsername().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TimesheetUser.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUser"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associatedProjects");
        elemField.setXmlName(new javax.xml.namespace.QName("", "associatedProjects"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetProject"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entries");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entries"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetWorkEntry"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordHash");
        elemField.setXmlName(new javax.xml.namespace.QName("", "passwordHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "roles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://timesheet.bme.hu/wsdl", "timesheetUserRole"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("username");
        elemField.setXmlName(new javax.xml.namespace.QName("", "username"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
