/**
 * ITimesheetDataInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package hu.bme.timesheet.wsdl;

public interface ITimesheetDataInterface extends java.rmi.Remote {
    public hu.bme.timesheet.wsdl.TimesheetUser getUserById(hu.bme.timesheet.wsdl.TimesheetUserSession session, long userId) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetUser getUserByName(hu.bme.timesheet.wsdl.TimesheetUserSession session, java.lang.String username) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetProject getProjectById(hu.bme.timesheet.wsdl.TimesheetUserSession session, long projectId) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
    public hu.bme.timesheet.wsdl.TimesheetProject getProjectByName(hu.bme.timesheet.wsdl.TimesheetUserSession session, java.lang.String projectName) throws java.rmi.RemoteException, hu.bme.timesheet.wsdl.TimesheetFault;
}
