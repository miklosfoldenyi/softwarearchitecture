import hu.bme.timesheet.wsdl.ITimesheetManagerInterface;
import hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator;
import hu.bme.timesheet.wsdl.TimesheetProject;
import hu.bme.timesheet.wsdl.TimesheetUser;
import hu.bme.timesheet.wsdl.TimesheetUserSession;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

/**
 * Servlet implementation class ManagerServlet
 */
@WebServlet("/manager")
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManagerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	private ITimesheetManagerInterface manager;

	private TimesheetUserSession sessionHash;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		manager = (ITimesheetManagerInterface) request.getSession().getAttribute("manager");
		if (manager == null) {
			try {
				manager = new TimesheetManagerServiceLocator().getTimesheetManagerPort();
				request.getSession().setAttribute("manager", manager);
			} catch (ServiceException e) {
				request.setAttribute("errormessage", e);
				request.getRequestDispatcher("faces/error.jsp").forward(request, response);
				return;
			}
		}

		sessionHash = (TimesheetUserSession) request.getSession().getAttribute("sessionHash");

		if (sessionHash == null) {
			response.sendRedirect("login");
			return;
		}

		// execute actions if any then reload
		if (doActions(request, response)) {
			response.sendRedirect("manager");
			return;
		}

		try {
			TimesheetUser[] allUsers = manager.listAllUsers(sessionHash);
			request.setAttribute("ts_users", allUsers);

			TimesheetProject[] allProjects = manager.listAllProjects(sessionHash);
			request.setAttribute("ts_projects", allProjects);

			Map<TimesheetUser, List<TimesheetProject>> projectMap = new HashMap<>();

			for (TimesheetUser user : allUsers) {
				List<TimesheetProject> prjs = new ArrayList<>();
				projectMap.put(user, prjs);
				if (user.getAssociatedProjects() == null)
					continue;
				for (TimesheetProject userProject : user.getAssociatedProjects()) {
					for (TimesheetProject project : allProjects) {
						if (userProject.getId() == project.getId()) {
							prjs.add(project);
							break;
						}
					}
				}
			}
			request.setAttribute("ts_user_project_map", projectMap);

		} catch (RemoteException e) {
			error(request, response, e);
			return;
		}

		request.getRequestDispatcher("faces/manager.jsp").forward(request, response);
	}

	private boolean doUserCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("create_user".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setName(request.getParameter("name"));
			user.setUsername(request.getParameter("username"));

			manager.createUser(sessionHash, user, request.getParameter("password"));
			return true;
		}

		return false;
	}

	private boolean doUserDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("user_delete".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setUsername(request.getParameter("username"));
			user.setId(Long.parseLong(request.getParameter("userid")));
			manager.deleteUser(sessionHash, user);
			return true;
		}

		return false;
	}

	private boolean doProjectCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("project_create".equals(request.getParameter("action"))) {
			TimesheetProject project = new TimesheetProject();
			project.setName(request.getParameter("name"));

			manager.createProject(sessionHash, project);
			return true;
		}

		return false;
	}

	private boolean doProjectDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("project_delete".equals(request.getParameter("action"))) {
			TimesheetProject project = new TimesheetProject();
			project.setId(Long.parseLong(request.getParameter("projectid")));

			manager.deleteProject(sessionHash, project);
			return true;
		}

		return false;
	}

	private boolean doAddUserToProject(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("add_user_to_project".equals(request.getParameter("action"))) {
			TimesheetProject project = new TimesheetProject();
			project.setId(Long.parseLong(request.getParameter("projectid")));

			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));

			manager.addUserToProject(sessionHash, project, user);
			return true;
		}

		return false;
	}

	private boolean doRemoveUserFromProject(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if ("remove_user_from_project".equals(request.getParameter("action"))) {
			TimesheetProject project = new TimesheetProject();
			project.setId(Long.parseLong(request.getParameter("projectid")));

			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));

			manager.removeUserFromProject(sessionHash, project, user);
			return true;
		}

		return false;
	}

	private boolean doGrantClient(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("grant_client".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));
			manager.grantClientRole(sessionHash, user);
			return true;
		}

		return false;
	}

	private boolean doGrantManager(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("grant_manager".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));
			manager.grantManageRole(sessionHash, user);
			return true;
		}

		return false;
	}

	private boolean doRevokeClient(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("revoke_client".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));
			manager.revokeClientRole(sessionHash, user);
			return true;
		}

		return false;
	}

	private boolean doRevokeManager(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		if ("revoke_manager".equals(request.getParameter("action"))) {
			TimesheetUser user = new TimesheetUser();
			user.setId(Long.parseLong(request.getParameter("userid")));
			manager.revokeManageRole(sessionHash, user);
			return true;
		}

		return false;
	}

	private void error(HttpServletRequest request, HttpServletResponse response, Object msg) throws ServletException,
			IOException {
		request.setAttribute("errormessage", msg);
		request.getRequestDispatcher("faces/error.jsp").forward(request, response);
	}

	private boolean doActions(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		boolean done = false;
		try {
			done = doGrantClient(request, response) ? true : done;
			done = doGrantManager(request, response) ? true : done;
			done = doRevokeClient(request, response) ? true : done;
			done = doRevokeManager(request, response) ? true : done;
			done = doUserCreate(request, response) ? true : done;
			done = doUserDelete(request, response) ? true : done;
			done = doProjectCreate(request, response) ? true : done;
			done = doProjectDelete(request, response) ? true : done;
			done = doAddUserToProject(request, response) ? true : done;
			done = doRemoveUserFromProject(request, response) ? true : done;

			return done;
		} catch (RemoteException e) {
			error(request, response, e);
			return true;
		}

	}

}
