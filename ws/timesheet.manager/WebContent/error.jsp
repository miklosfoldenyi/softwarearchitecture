<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser - Hiba t�rt�nt!</title>
</head>
<body>
	<div class="topbar">
		<%
			if (request.getSession().getAttribute("sessionHash") != null) {
		%>
		<a href="manager">Menedzser</a> <span>|</span> <a href="report">Riport
			gener�l�s</a> <span>|</span> <a href="logout">Kijelentkez�s</a>
		<%
			} else {
		%>
		<a href="login">Bejelentkez�s</a>
		<%
			}
		%>
	</div>
	<div class="headerbar">
		<h1 class="logo">Hiba t�rt�nt!</h1>
	</div>
	<div class="content">
		<div class="contentshadow"></div>

		<h2><%=request.getAttribute("errormessage")%></h2>
		<ul>
			<%
				if (request.getSession().getAttribute("sessionHash") != null) {
			%>
			<li><a href="logout">Kijelentkez�s</a></li>
			<li><a href="manager">Menedzsment</a></li>
			<%
				} else {
			%>
			<li><a href="login">Bejelentkez�s</a></li>
			<%
				}
			%>
		</ul>
	</div>
</body>
</html>