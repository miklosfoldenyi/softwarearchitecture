<%@page import="hu.bme.timesheet.wsdl.TimesheetUser"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUserSession"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator"%>
<%@page import="hu.bme.timesheet.wsdl.ITimesheetManagerInterface"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser</title>
</head>
<body>
	<div class="topbar"></div>
	<div class="headerbar"><h1 class="logo">Timesheet</h1></div>
	<div class="content">
		<div class="contentshadow"></div>
		<f:view>
			<form class="login" method="get">
				<table>
					<tr>
						<td colspan="2" class="centered label">L�pj be!</td>
					</tr>
					<tr>
						<td>Felhaszn�l�n�v:</td>
						<td><input name="username" type="text" /></td>
					</tr>
					<tr>
						<td>Jelsz�:</td>
						<td><input name="password" type="password" /></td>
					</tr>
					<tr>
						<td colspan="2" class="centered"><input class="button" type="submit" name="" value="Bel�pek" /></td>
					</tr>
				</table>
			</form>

			<h:outputText value="#{managerUIBean.message}" />

			<h:outputText value="#{managerUIBean.users}" />
		</f:view>
	</div>
	<div class="contentshadow"></div>
</body>
</html>