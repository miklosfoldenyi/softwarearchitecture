<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser</title>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {
		'packages' : [ 'corechart' ]
	});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(drawChart);

	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	function drawChart() {


		<%List<TimesheetUser> users = (List<TimesheetUser>)request.getAttribute("ts_users");
		Map<Long, Long> workMapByUserId = (Map<Long, Long>) request.getAttribute("ts_work_map");
		String projectString = (String)request.getAttribute("ts_projects_string"); %>
		
		
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Felhasználó');
		data.addColumn('number', 'Munkaidő');
		data.addRows([
 <%for (int i = 0; i < users.size(); i++ ) {
	TimesheetUser user =  users.get(i);
	
	if (i != 0) {%>, <%}%>['<%=user.getName()%>', <%=workMapByUserId.get(user.getId()).toString()%>] <%}%>

	]);

		// Set chart options
		var options = {
			'title' : 'Munkamegosztás (<%=projectString%>)',
			'width' : 400,
			'height' : 300
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document
				.getElementById('chart_div'));
		chart.draw(data, options);
	}
</script>
</head>
<body>
<div class="topbar">
	<a href="manager">Menedzser</a> <span>|</span> <a href="report">Riport generálás</a> <span>|</span> <a href="logout">Kijelentkezés</a>
</div>
<div class="headerbar">
	<h1 class="logo">Timesheet</h1>
</div>
<div class="content2">
	<div class="contentshadow"></div>
	<div class="paddinged">
		<div class="editor">
			<!--Div that will hold the pie chart-->
			<div id="chart_div"></div>
		</div>
	</div>
</div>
<div class="contentshadow"></div>
</body>
</html>