<%@page import="hu.bme.timesheet.wsdl.TimesheetProject"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUserRole"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser</title>
</head>
<body>
	<div class="topbar">
		<a href="manager">Menedzser</a> <span>|</span> <a href="logout">Kijelentkez�s</a>
	</div>
	<div class="headerbar">
		<h1 class="logo">Timesheet</h1>
	</div>
	<div class="content2">
		<div class="contentshadow"></div>
		<div class="paddinged">
			<div class="editor">
				<h2>Felhaszn�l�i fi�kok</h2>
				<form>
					<table>
						<tr>
							<th>N�v</th>
							<th>Jelent�s?</th>
						</tr>
						<%
							TimesheetUser[] users = (TimesheetUser[]) request.getAttribute("ts_users");

							for (TimesheetUser user : users) {
								TimesheetUserRole[] roles = user.getRoles();
								boolean clientRole = false;

								for (int i = 0; roles != null && i < roles.length; i++) {
									if (roles[i].getRoleName().equals("client")) {
										clientRole = true;
									}
								}
								if (!clientRole) {
									continue;
								}
						%>
						<tr>
							<td><%=user.getName()%></td>
							<td><input type="checkbox" name="userid"
								value="<%=user.getId()%>" /></td>
						</tr>
						<%
							}
						%>
					</table>
					<h2>Projektek</h2>
					<table>
						<tr>
							<th>N�v</th>
							<th>Jelent�s?</th>
						</tr>
						<%
							TimesheetProject[] projects = (TimesheetProject[]) request.getAttribute("ts_projects");

							for (TimesheetProject project : projects) {
						%>
						<tr>
							<td><%=project.getName()%></td>
							<td><input type="checkbox" name="projectid"
								value="<%=project.getId()%>" /></td>						
						</tr>
						<%
							}
						%>
					</table>
					<div
						style="text-align: left; width: 120px; margin: 0 auto; margin-top: 10px">
						<input type="hidden" name="action" value="generate" /> <input
							style="width: 30px" type="radio" name="reporttype" value="graph"
							SELECTED />Grafikon<br> <input style="width: 30px"
							type="radio" name="reporttype" value="table" />T�bl�zat<br>
					</div>
					<input class="button" type="submit" value="Riport Gener�l�s" />
				</form>
			</div>
		</div>
	</div>
	</div>
	<div class="contentshadow"></div>
</body>
</html>