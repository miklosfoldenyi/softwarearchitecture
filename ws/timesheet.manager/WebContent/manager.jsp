<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetProject"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUserRole"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUser"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUserSession"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetManagerServiceLocator"%>
<%@page import="hu.bme.timesheet.wsdl.ITimesheetManagerInterface"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if (session.getAttribute("sessionHash") == null) {
		response.sendRedirect("login");
		return;
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser</title>
</head>
<body>
	<div class="topbar">
		<a href="report">Riport gener�l�s</a> <span>|</span> <a href="logout">Kijelentkez�s</a>
	</div>
	<div class="headerbar">
		<h1 class="logo">Timesheet</h1>
	</div>
	<div class="content2">
		<div class="contentshadow"></div>
		<div class="paddinged">
			<div class="editor">
				<h2>Felhaszn�l�i fi�kok</h2>
				<table>
					<tr>
						<th>&nbsp;Azonos�t�&nbsp;</th>
						<th>&nbsp;N�v&nbsp;</th>
						<th>&nbsp;Felhaszn�l� n�v&nbsp;</th>
						<th>&nbsp;Android&nbsp;</th>
						<th>&nbsp;Manager&nbsp;</th>
						<th>&nbsp;T�rl�s&nbsp;</th>
					</tr>
					<%
						TimesheetUser[] users = (TimesheetUser[]) request.getAttribute("ts_users");

						for (TimesheetUser user : users) {
					%>
					<tr>
						<%
							TimesheetUserRole[] roles = user.getRoles();
								String clientRole = "<a href=\"manager?action=grant_client&userid=" + user.getId() + "\">&#10007;</a>";
								String managerRole = "<a href=\"manager?action=grant_manager&userid=" + user.getId()
										+ "\">&#10007;</a>";

								for (int i = 0; roles != null && i < roles.length; i++) {
									if (roles[i].getRoleName().equals("client")) {
										clientRole = "<a href=\"manager?action=revoke_client&userid=" + user.getId()
												+ "\">&#10003;</a>";
									}
									if (roles[i].getRoleName().equals("manager")) {
										managerRole = "<a href=\"manager?action=revoke_manager&userid=" + user.getId()
												+ "\">&#10003;</a>";
									}
								}
						%>


						<td><%=user.getId()%></td>
						<td><%=user.getName()%></td>
						<td><%=user.getUsername()%></td>
						<td><%=clientRole%></td>
						<td><%=managerRole%></td>
						<td><a
							href="manager?action=user_delete&userid=<%=user.getId()%>&username=<%=user.getUsername()%>">T�rl�s</a></td>
					</tr>
					<%
						}
					%>
				</table>
				<form class="manager">
					<table>
						<tr>
							<td>N�v</td>
							<td><input type="hidden" name="action" value="create_user" /><input
								type="text" name="name" /></td>
						</tr>
						<tr>
							<td style="padding-right: 10px">Felhaszn�l�n�v</td>
							<td><input name="username" type="text" /></td>
						</tr>
						<tr>
							<td>Jelsz�</td>
							<td><input name="password" type="password" /></td>
						</tr>
						<tr>
							<td colspan="2" class="centered"><input class="button"
								type="submit" value="�j felhaszn�l� l�trehoz�sa" /></td>
						</tr>
					</table>
				</form>
			</div>

			<div class="editor">
				<h2>Projektek</h2>
				<table>
					<tr>

						<th>Azonos�t�</th>
						<th>N�v</th>
						<th>T�rl�s</th>
					</tr>
					<%
						TimesheetProject[] projects = (TimesheetProject[]) request.getAttribute("ts_projects");

						for (TimesheetProject project : projects) {
					%>
					<tr>
						<td><%=project.getId()%></td>
						<td><%=project.getName()%></td>
						<td><a
							href="manager?action=project_delete&projectid=<%=project.getId()%>">T�rl�s</a></td>
					</tr>
					<%
						}
					%>
				</table>
				<form class="manager">
					<table>
						<tr>
							<td style="width: 117px">N�v</td>
							<td><input type="hidden" name="action"
								value="project_create" /><input type="text" name="name" /></td>
						</tr>
						<tr>
							<td colspan="2" class="centered"><input class="button"
								type="submit" value="�j projekt l�trehoz�sa" /></td>
						</tr>
					</table>
				</form>
			</div>

			<div class="editor">
				<h2>Felhaszn�l�/Projekt �sszerendel�s</h2>
				<table>
					<tr>

						<th>Felhaszn�l� (azonos�t�)</th>
						<%
							Map<TimesheetUser, List<TimesheetProject>> map = (Map<TimesheetUser, List<TimesheetProject>>) request
									.getAttribute("ts_user_project_map");
							for (TimesheetProject prj : (TimesheetProject[]) request.getAttribute("ts_projects")) {
						%><th>&nbsp;<%=prj.getName()%>&nbsp;
						</th>
						<%
							}
						%>

					</tr>
					<%
						for (TimesheetUser user : (TimesheetUser[]) request.getAttribute("ts_users")) {
					%>
					<tr>
						<td><%=user.getName()%> (<%=user.getId()%>)</td>
						<%
							for (TimesheetProject prj : (TimesheetProject[]) request.getAttribute("ts_projects")) {

									String assigned = "<a href=\"manager?action=add_user_to_project&userid=" + user.getId()
											+ "&projectid=" + prj.getId() + "\">&#10007;</a>";
									if (map.get(user).contains(prj)) {
										assigned = "<a href=\"manager?action=remove_user_from_project&userid=" + user.getId()
												+ "&projectid=" + prj.getId() + "\">&#10003;</a>";
									}
						%>
						<td><%=assigned%></td>
						<%
							}
						%>
					</tr>
					<%
						}
					%>
				</table>
			</div>
		</div>
	</div>
	<div class="contentshadow"></div>
</body>
</html>