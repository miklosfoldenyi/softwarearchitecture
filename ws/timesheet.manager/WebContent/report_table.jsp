<%@page import="hu.bme.timesheet.wsdl.TimesheetProject"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="hu.bme.timesheet.wsdl.TimesheetUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
<link rel="stylesheet" href="style.css">
<title>Timesheet Menedzser - Riport</title>
</head>
<body>
<div class="topbar">
	<a href="manager">Menedzser</a> <span>|</span> <a href="report">Riport gener�l�s</a> <span>|</span> <a href="logout">Kijelentkez�s</a>
</div>

<div class="headerbar">
	<h1 class="logo">Timesheet</h1>
</div>
<%
	List<TimesheetUser> users = (List<TimesheetUser>) request.getAttribute("ts_users");
	List<TimesheetProject> projects = (List<TimesheetProject>) request.getAttribute("ts_projects");
	Map<Long, Long> userTotal = (Map<Long, Long>) request.getAttribute("ts_user_total");
	Map<Long, Long> projectTotal = (Map<Long, Long>) request.getAttribute("ts_project_total");
	Long total = (Long) request.getAttribute("ts_total");
	Map<Long, Map<Long, Long>> workMapByUserId = (Map<Long, Map<Long, Long>>) request
			.getAttribute("ts_work_map");
%>

<div class="content2">
	<div class="contentshadow"></div>
	<div class="paddinged">
		<div class="editor">
			<table>
				<tr>
					<th>Felhaszn�l�k \ Projektek</th>
					<%
						for (TimesheetProject proj : projects) {
					%><th>&nbsp;<%=proj.getName()%>&nbsp;
					</th>
					<%
						}
					%>
				</tr>
				<%
					for (TimesheetUser user : users) {
				%>
				<tr>
					<td><%=user.getName()%> (<%=userTotal.get(user.getId()) * 100 / total%>%)</td>
					<%
						for (TimesheetProject proj : projects) {
								Long time = workMapByUserId.get(user.getId()).get(proj.getId());
					%>
					<td>
						<%
							if (time == 0) {
						%>&nbsp;-&nbsp; <%
 	} else {
 %> <%=time / 3600%>:<%=String.format("%02d", time % 3600 / 60)%>:<%=String.format("%02d", time % 3600 % 60)%> (<%=time * 100 / projectTotal.get(proj.getId())%>%)
						<%
 	}
 %>
					</td>
					<%
						}
					%>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	</div>
</div>
<div class="contentshadow"></div>
</body>
</html>