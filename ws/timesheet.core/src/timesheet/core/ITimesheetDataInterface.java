package timesheet.core;

import javax.jws.WebParam;
import javax.jws.WebService;

import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserSession;

@WebService(targetNamespace = "http://timesheet.bme.hu/wsdl")
public interface ITimesheetDataInterface {
	TimesheetUser getUserByName(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "username") String username) throws TimesheetFault;

	TimesheetUser getUserById(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "userId") long id) throws TimesheetFault;

	TimesheetProject getProjectByName(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "projectName") String projectName) throws TimesheetFault;

	TimesheetProject getProjectById(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "projectId") long projectId) throws TimesheetFault;

}
