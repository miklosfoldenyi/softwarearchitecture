package timesheet.core;

import java.util.Collection;

import javax.jws.WebParam;
import javax.jws.WebService;

import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserSession;
import timesheet.model.TimesheetWorkEntry;

@WebService(targetNamespace = "http://timesheet.bme.hu/wsdl")
public interface ITimesheetClientInterface {

	/**
	 * Authenticates the user. This is a prerequisite to all listing and editing
	 * operations via clients.
	 * 
	 * @param username
	 * @param password
	 * @return a session id to be used in future calls.
	 */
	TimesheetUserSession login(@WebParam(name = "user") TimesheetUser user, @WebParam(name = "password") String password)
			throws TimesheetFault;

	boolean logout(@WebParam(name = "session") TimesheetUserSession session) throws TimesheetFault;

	Collection<TimesheetProject> getAssociatedProjects(@WebParam(name = "session") TimesheetUserSession session)
			throws TimesheetFault;

	Collection<TimesheetWorkEntry> getAssociatedEntries(@WebParam(name = "session") TimesheetUserSession session)
			throws TimesheetFault;

	boolean addWork(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "workEntry") TimesheetWorkEntry entry) throws TimesheetFault;

	boolean addUserToWork(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "workEntry") TimesheetWorkEntry entry, @WebParam(name = "user") TimesheetUser user)
			throws TimesheetFault;

	Collection<TimesheetUser> getUsersForProject(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "project") TimesheetProject project) throws TimesheetFault;
}
