package timesheet.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import timesheet.core.util.Hasher;
import timesheet.exceptions.AuthenticationException;
import timesheet.exceptions.BackendException;
import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserRole;
import timesheet.model.TimesheetUserSession;

@Stateless
public class Authentication {

	public static final String CLIENT_ROLE_STRING = "client";
	public static final String MANAGER_ROLE_STRING = "manager";

	@PersistenceContext(unitName = "TimesheetPU")
	private EntityManager em;

	/**
	 * Authenticates the username and password.
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws TimesheetAuthenticationException
	 */
	public TimesheetUserSession login(String username, String password) throws TimesheetFault {
		initDB();

		Query query = em.createQuery("Select o from " + TimesheetUser.class.getSimpleName()
				+ " o where o.username = :username");
		query.setParameter("username", username);

		TimesheetUserSession session = new TimesheetUserSession();
		session.setStartTime(new Date());

		@SuppressWarnings("unchecked")
		List<TimesheetUser> users = query.getResultList();

		if (users.size() != 1) {
			session.setNote("Login failed. Username does not exist.");
			session.setEndTime(new Date());
			em.persist(session);
			throw new TimesheetFault(Strings.LOGIN_FAILED + Strings.AUTHENTICATION_FAILED);
		}

		TimesheetUser user = users.get(0);
		session.setUser(user);
		session.setSessionHash(getSessionHash(user, session.getStartTime()));

		if (!user.getPasswordHash().equals(getPasswordHash(user, password))) {
			session.setNote("Login failed. Password does not match.");
			session.setEndTime(new Date());
			em.persist(session);
			throw new TimesheetFault(Strings.LOGIN_FAILED + Strings.AUTHENTICATION_FAILED);
		}

		em.persist(session);

		return session;
	}

	private Hasher h = Hasher.getHasher(Hasher.SHA1_PROTOCOLL);

	private String getSessionHash(TimesheetUser user, Date date) {
		return h.hash(user.getUsername() + ":" + date.getTime());
	}

	public String getPasswordHash(TimesheetUser user, String password) {
		return h.hash(user.getUsername() + ":" + password);
	}

	private void initDB() {
		Query query = em.createQuery("Select o from " + TimesheetUser.class.getSimpleName()
				+ " o where o.username = :username");
		query.setParameter("username", "admin");

		if (query.getResultList().size() == 0) {
			TimesheetUserRole clientRole = new TimesheetUserRole();
			clientRole.setRoleName("client");
			em.persist(clientRole);

			TimesheetUserRole managerRole = new TimesheetUserRole();
			managerRole.setRoleName("manager");
			em.persist(managerRole);

			TimesheetUser admin = new TimesheetUser();
			admin.setName("Administrator");
			admin.setUsername("admin");
			admin.setPasswordHash(getPasswordHash(admin, "admin"));
			admin.setRoles(new ArrayList<TimesheetUserRole>());
			admin.getRoles().add(managerRole);
			em.persist(admin);
			em.flush();
		}
	}

	public boolean hasRole(TimesheetUser user, String role) {

		if (user == null || user.getRoles() == null) {
			return false;
		}

		for (TimesheetUserRole r : user.getRoles()) {
			if (r.getRoleName().equals(role)) {
				return true;
			}
		}

		return false;
	}

	public TimesheetUserSession getUserSessionByHash(String sessionHash) throws TimesheetFault {
		Query query = em.createQuery("Select o from " + TimesheetUserSession.class.getSimpleName()
				+ " o where o.sessionHash = :hash and o.endTime IS NULL");
		query.setParameter("hash", sessionHash);

		if (query.getResultList().size() == 0) {
			throw new TimesheetFault(Strings.NO_SUCH_SESSION);
		}

		TimesheetUserSession session = (TimesheetUserSession) query.getSingleResult();

		return session;
	}

	public TimesheetUser getUser(TimesheetUser user) throws TimesheetFault {
		Query query = em.createQuery("Select o from " + TimesheetUser.class.getSimpleName()
				+ " o where o.username = :username or o.id = :id");
		String username = user.getUsername() == null ? "XX" : user.getUsername();

		query.setParameter("username", username);
		query.setParameter("id", user.getId());

		if (query.getResultList().size() == 0) {
			throw new TimesheetFault(Strings.NO_SUCH_USER);
		}

		return (TimesheetUser) query.getResultList().get(0);
	}

	public TimesheetUserRole getRole(String roleName) throws TimesheetFault {
		Query query = em.createQuery("Select o from " + TimesheetUserRole.class.getSimpleName()
				+ " o where o.roleName = :roleName");
		query.setParameter("roleName", roleName);

		if (query.getResultList().size() == 0) {
			throw new TimesheetFault(Strings.NO_SUCH_ROLE);
		}

		return (TimesheetUserRole) query.getResultList().get(0);
	}

	public TimesheetProject getProject(TimesheetProject project) throws TimesheetFault {
		Query query = em.createQuery("Select o from " + TimesheetProject.class.getSimpleName()
				+ " o where o.name = :name or o.id = :id");

		String projectName = project.getName() == null ? "XXX" : project.getName();
		query.setParameter("name", projectName);
		query.setParameter("id", project.getId());

		if (query.getResultList().size() == 0) {
			throw new TimesheetFault(Strings.NO_SUCH_PROJECT);
		}

		return (TimesheetProject) query.getResultList().get(0);
	}

	public TimesheetUserSession checkSessionCredentials(TimesheetUserSession callInSession, Collection<String> roles)
			throws TimesheetFault {
		if (callInSession == null) {
			throw new TimesheetFault(Strings.NO_SUCH_SESSION);
		}
		Query query = em.createQuery("Select o from " + TimesheetUserSession.class.getSimpleName()
				+ " o where o.sessionHash = :hash and o.endTime IS NULL");
		query.setParameter("hash", callInSession.getSessionHash());

		if (query.getResultList().size() != 1) {
			throw new TimesheetFault(Strings.NO_SUCH_SESSION);
		}

		TimesheetUserSession localSession = (TimesheetUserSession) query.getResultList().get(0);

		if (localSession == null) {
			throw new TimesheetFault(Strings.NO_SUCH_SESSION);
		}

		for (String role : roles) {
			if (!hasRole(localSession.getUser(), role)) {
				throw new TimesheetFault(Strings.LOGIN_FAILED + Strings.AUTHENTICATION_FAILED);
			}
		}

		return localSession;
	}

	public TimesheetUserSession checkSessionCredentials(TimesheetUserSession callInSession, String role)
			throws TimesheetFault {
		Collection<String> roles = new ArrayList<>();
		roles.add(role);
		return checkSessionCredentials(callInSession, roles);
	}

	public TimesheetUserSession checkSessionCredentials(TimesheetUserSession callInSession) throws TimesheetFault {
		return checkSessionCredentials(callInSession, new ArrayList<String>());
	}

}