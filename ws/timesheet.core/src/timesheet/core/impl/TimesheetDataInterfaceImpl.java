package timesheet.core.impl;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import timesheet.core.ITimesheetDataInterface;
import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserSession;

@Stateless
@WebService(portName = "TimesheetDataPort", serviceName = "TimesheetDataService", targetNamespace = "http://timesheet.bme.hu/wsdl", endpointInterface = "timesheet.core.ITimesheetDataInterface")
public class TimesheetDataInterfaceImpl implements ITimesheetDataInterface {

	@Inject
	private Authentication authenticator;

	@PersistenceContext(unitName = "TimesheetPU")
	private EntityManager em;

	@Override
	public TimesheetUser getUserByName(@WebParam(name = "session") TimesheetUserSession session, String username)
			throws TimesheetFault {
		authenticator.checkSessionCredentials(session);

		Map<String, Object> map = new HashMap<>();
		map.put("username", username);

		return getObject(TimesheetUser.class, map);
	}

	@Override
	public TimesheetUser getUserById(@WebParam(name = "session") TimesheetUserSession session, long id)
			throws TimesheetFault {
		authenticator.checkSessionCredentials(session);

		Map<String, Object> map = new HashMap<>();
		map.put("id", (Long)id);

		return getObject(TimesheetUser.class, map);
	}

	@Override
	public TimesheetProject getProjectByName(TimesheetUserSession session, String projectName) throws TimesheetFault {
		authenticator.checkSessionCredentials(session);

		Map<String, Object> map = new HashMap<>();
		map.put("name", "" + projectName);

		return getObject(TimesheetProject.class, map);
	}
	
	@Override
	public TimesheetProject getProjectById(TimesheetUserSession session, long projectId) throws TimesheetFault {
		authenticator.checkSessionCredentials(session);

		Map<String, Object> map = new HashMap<>();
		map.put("id", projectId);

		return getObject(TimesheetProject.class, map);
	}

	private <T> T getObject(Class<T> clazz, Map<String, Object> keys) throws TimesheetFault {

		StringBuilder sb = new StringBuilder();
		sb.append(" where ");

		for (String key : keys.keySet()) {
			if (sb.toString().length() == 7) {
				sb.append(" o." + key + " = :" + key);
			} else {
				sb.append(" and o." + key + " = :" + key);
			}
		}

		Query query = em.createQuery("Select o from " + clazz.getSimpleName() + " o " + sb.toString());

		for (String key : keys.keySet()) {
			query.setParameter(key, keys.get(key));
		}

		if (query.getResultList().size() > 0) {
			return (T) query.getResultList().get(0);
		}

		throw new TimesheetFault("No such element");
	}

}
