package timesheet.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import timesheet.core.ITimesheetManagerInterface;
import timesheet.exceptions.AuthenticationException;
import timesheet.exceptions.InvalidOperationException;
import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetSessionType;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserRole;
import timesheet.model.TimesheetUserSession;
import timesheet.model.TimesheetWorkEntry;

@Stateless
@WebService(portName = "TimesheetManagerPort", serviceName = "TimesheetManagerService", targetNamespace = "http://timesheet.bme.hu/wsdl", endpointInterface = "timesheet.core.ITimesheetManagerInterface")
public class TimesheetManagerInterfaceImpl implements ITimesheetManagerInterface {

	private static final String REQUIRED_ROLE = Authentication.MANAGER_ROLE_STRING;

	@Inject
	private Authentication authenticator;

	@PersistenceContext(unitName = "TimesheetPU")
	private EntityManager em;

	@Override
	public TimesheetUserSession login(TimesheetUser user, String password) throws TimesheetFault {
		TimesheetUserSession session = authenticator.login(user.getUsername(), password);

		session.setType(TimesheetSessionType.MANAGEMENT_SESSION);

		if (!authenticator.hasRole(session.getUser(), REQUIRED_ROLE)) {
			session.setEndTime(new Date());
			session.setNote(Strings.LOGIN_FAILED + Strings.MISSING_ROLE);
			em.persist(session);
			throw new TimesheetFault(Strings.LOGIN_FAILED + Strings.MISSING_ROLE);
		} else {
			em.persist(session);
			return session.safeClone();
		}
	}

	@Override
	public boolean logout(TimesheetUserSession session) throws TimesheetFault {
		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		if (localSession.getType() == TimesheetSessionType.MANAGEMENT_SESSION) {
			localSession.setEndTime(new Date());
			localSession.setNote("Logout.");
			em.persist(localSession);
			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_SESSION_TYPE);
		}
	}

	@Override
	public boolean createUser(TimesheetUserSession session, TimesheetUser user, String initialPassword)
			throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		if (user == null) {
			throw new TimesheetFault("Adjon meg a l�trehozand� szem�ly nev�t �s falhaszn�l�i azonos�t�j�t.");
		}

		Query query = em.createQuery("Select o from " + TimesheetUser.class.getSimpleName()
				+ " o where o.username = :username");
		query.setParameter("username", user.getUsername());

		if (query.getResultList().size() > 0) {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_USERNAME_TAKEN);
		}

		if (user.getUsername().length() < 3) {
			throw new TimesheetFault("A felhaszn�l� n�v t�l r�vid!");
		}

		if (initialPassword.length() < 4) {
			throw new TimesheetFault("A jelsz� t�l r�vid!");
		}

		if (user.getName().length() < 6) {
			throw new TimesheetFault("A megadott n�v t�l r�vid!");
		}

		TimesheetUser newUser = new TimesheetUser();
		newUser.setName(user.getName());
		newUser.setUsername(user.getUsername());
		newUser.setPasswordHash(authenticator.getPasswordHash(newUser, initialPassword));
		newUser.setEntries(new ArrayList<TimesheetWorkEntry>());
		em.persist(newUser);
		return true;

	}

	@Override
	public boolean deleteUser(TimesheetUserSession session, TimesheetUser user) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		Query query = em.createQuery("Select DISTINCT(o) from " + TimesheetUser.class.getSimpleName()
				+ " o where o.username = :username or o.id = :id");
		query.setParameter("username", user.getUsername());
		query.setParameter("id", user.getId());

		if (query.getResultList().size() == 0) {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		} else {
			em.remove(query.getSingleResult());
			return true;
		}
	}

	private boolean grantRole(TimesheetUserSession session, TimesheetUser user, String roleName) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetUserRole role = authenticator.getRole(roleName);
		TimesheetUser usr = authenticator.getUser(user);

		if (!usr.getRoles().contains(role)) {
			usr.getRoles().add(role);
			em.persist(usr);
			em.persist(role);
			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		}

	}

	private boolean revokeRole(TimesheetUserSession session, TimesheetUser user, String roleName) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetUserRole role = authenticator.getRole(roleName);
		TimesheetUser usr = authenticator.getUser(user);

		if ("admin".equals(usr.getUsername()) && Authentication.MANAGER_ROLE_STRING.equals(role.getRoleName())) {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		}

		if (usr.getRoles().contains(role)) {
			usr.getRoles().remove(role);
			em.persist(usr);
			em.persist(role);
			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		}

	}

	@Override
	public Collection<TimesheetUser> listAllUsers(TimesheetUserSession session) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		Query query = em.createQuery("Select DISTINCT(o) from " + TimesheetUser.class.getSimpleName()
				+ " o left join fetch o.roles left join fetch o.associatedProjects order by o.id");
		List<TimesheetUser> users = new ArrayList<>();

		for (TimesheetUser user : (List<TimesheetUser>) query.getResultList()) {
			em.detach(user);
			user.setPasswordHash(null);
			if (user.getRoles() != null) {
				Iterator<TimesheetUserRole> iterator = user.getRoles().iterator();
				while (iterator.hasNext()) {
					iterator.next();
				}
			}
			users.add(user);
		}
		return users;
	}

	@Override
	public boolean createProject(TimesheetUserSession session, TimesheetProject project) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		Query query = em.createQuery("Select o from " + TimesheetProject.class.getSimpleName()
				+ " o where o.name = :name");
		query.setParameter("name", project.getName());

		if (project.getName().length() < 3) {
			throw new TimesheetFault("A megadott n�v t�l r�vid!");
		}

		if (query.getResultList().size() == 0) {
			TimesheetProject prj = new TimesheetProject();
			prj.setName(project.getName());
			em.persist(prj);

			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		}
	}

	@Override
	public boolean deleteProject(TimesheetUserSession session, TimesheetProject project) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetProject prj = authenticator.getProject(project);

		if (prj != null) {
			em.remove(prj);
			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN);
		}
	}

	@Override
	public boolean grantClientRole(TimesheetUserSession session, TimesheetUser user) throws TimesheetFault {
		return grantRole(session, user, Authentication.CLIENT_ROLE_STRING);
	}

	@Override
	public boolean revokeClientRole(TimesheetUserSession session, TimesheetUser user) throws TimesheetFault {
		return revokeRole(session, user, Authentication.CLIENT_ROLE_STRING);
	}

	@Override
	public boolean grantManageRole(TimesheetUserSession session, TimesheetUser user) throws TimesheetFault {
		return grantRole(session, user, Authentication.MANAGER_ROLE_STRING);
	}

	@Override
	public boolean revokeManageRole(TimesheetUserSession session, TimesheetUser user) throws TimesheetFault {
		return revokeRole(session, user, Authentication.MANAGER_ROLE_STRING);
	}

	@Override
	public Collection<TimesheetProject> listAllProjects(TimesheetUserSession session) throws TimesheetFault {

		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		Query query = em.createQuery("Select o from " + TimesheetProject.class.getSimpleName() + " o");
		List<TimesheetProject> projects = new ArrayList<>();

		for (TimesheetProject project : (List<TimesheetProject>) query.getResultList()) {
			em.detach(project);
			projects.add(project);
		}
		return projects;
	}

	@Override
	public boolean addUserToProject(TimesheetUserSession session, TimesheetProject project, TimesheetUser user)
			throws TimesheetFault {

		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetUser usr = em.find(TimesheetUser.class, user.getId());

		TimesheetProject prj = em.find(TimesheetProject.class, project.getId());

		if (usr != null && prj != null) {
			usr.getAssociatedProjects().add(prj);
			em.persist(usr);
			em.persist(prj);
			return true;
		} else {
			if (usr == null) {
				throw new TimesheetFault(Strings.NO_SUCH_USER);
			}
			if (prj == null) {
				throw new TimesheetFault(Strings.NO_SUCH_PROJECT);
			}
			return false;
		}
	}

	@Override
	public boolean removeUserFromProject(TimesheetUserSession session, TimesheetProject project, TimesheetUser user)
			throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetUser usr = em.find(TimesheetUser.class, user.getId());

		TimesheetProject prj = em.find(TimesheetProject.class, project.getId());

		if (usr != null && prj != null) {
			usr.getAssociatedProjects().remove(prj);
			em.persist(usr);
			em.persist(prj);
			return true;
		} else {
			if (usr == null) {
				throw new TimesheetFault(Strings.NO_SUCH_USER);
			}
			if (prj == null) {
				throw new TimesheetFault(Strings.NO_SUCH_PROJECT);
			}
			return false;
		}
	}

	@Override
	public Collection<TimesheetWorkEntry> listEntriesForUsersAndProjects(TimesheetUserSession session,
			Collection<TimesheetUser> users, Collection<TimesheetProject> projects) throws TimesheetFault {
		authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		List<TimesheetUser> localUsers = new ArrayList<>();
		List<TimesheetProject> localProjects = new ArrayList<>();

		for (TimesheetUser usr : users) {
			localUsers.add(authenticator.getUser(usr));
		}

		for (TimesheetProject prj : projects) {
			localProjects.add(authenticator.getProject(prj));
		}

		List<TimesheetWorkEntry> entries = new ArrayList<>();

		for (TimesheetUser queryUser : localUsers) {
			Query query = em
					.createQuery("Select o from "
							+ TimesheetWorkEntry.class.getSimpleName()
							+ " o left join fetch o.attendees left join fetch o.associatedProject where o.associatedProject IN :projects AND :user MEMBER OF o.attendees");
			query.setParameter("projects", localProjects);
			query.setParameter("user", queryUser);

			for (TimesheetWorkEntry entry : (List<TimesheetWorkEntry>) query.getResultList()) {
				if (!entries.contains(entry)) {
					entries.add(entry);
				}
			}

			for (TimesheetWorkEntry entry : entries) {
				em.detach(entry);
				for (TimesheetUser u : entry.getAttendees()) {
					em.detach(u);
					u.setPasswordHash(null);
				}
			}
		}

		return entries;
	}
}
