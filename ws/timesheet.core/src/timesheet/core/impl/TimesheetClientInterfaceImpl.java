package timesheet.core.impl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import timesheet.core.ITimesheetClientInterface;
import timesheet.exceptions.AuthenticationException;
import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetSessionType;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserSession;
import timesheet.model.TimesheetWorkEntry;

@Stateless
@WebService(portName = "TimesheetClientPort", serviceName = "TimesheetClientService", targetNamespace = "http://timesheet.bme.hu/wsdl", endpointInterface = "timesheet.core.ITimesheetClientInterface")
public class TimesheetClientInterfaceImpl implements ITimesheetClientInterface {

	public static final String REQUIRED_ROLE = Authentication.CLIENT_ROLE_STRING;

	@Inject
	private Authentication authenticator;

	@PersistenceContext(unitName = "TimesheetPU")
	private EntityManager em;

	@Override
	public TimesheetUserSession login(TimesheetUser user, String password) throws TimesheetFault {

		TimesheetUserSession session = authenticator.login(user.getUsername(), password);

		session.setType(TimesheetSessionType.CLIENT_SESSION);

		if (!authenticator.hasRole(session.getUser(), REQUIRED_ROLE)) {
			session.setEndTime(new Date());
			session.setNote("User login denied. Do not have role.");
			em.persist(session);
			throw new TimesheetFault("Nincs jogod a belépésre.", new AuthenticationException("Nem birtoklod a '" + REQUIRED_ROLE
					+ "' szerepet."));
		} else {
			em.persist(session);
			return session.safeClone();
		}
	}

	@Override
	public boolean logout(TimesheetUserSession session) throws TimesheetFault {
		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		if (localSession.getType() == TimesheetSessionType.CLIENT_SESSION) {
			localSession.setEndTime(new Date());
			localSession.setNote("Logout.");
			em.persist(localSession);
			return true;
		} else {
			throw new TimesheetFault(Strings.INVALID_SESSION_TYPE);
		}
	}

	@Override
	public Collection<TimesheetProject> getAssociatedProjects(TimesheetUserSession session) throws TimesheetFault {

		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		return localSession.getUser().getAssociatedProjects();
	}

	@Override
	public boolean addWork(TimesheetUserSession session, TimesheetWorkEntry entry) throws TimesheetFault {
		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		// get local project
		TimesheetProject localProject = em.find(TimesheetProject.class, entry.getAssociatedProject().getId());

		if (localProject == null) {
			throw new TimesheetFault(Strings.NO_SUCH_PROJECT);
		}

		if (!localProject.getAssociatedWorkers().contains(localSession.getUser())) {
			throw new TimesheetFault(Strings.USER_AND_PROJECT_ARE_NOT_ASSOCIATED);
		}
		
		if (entry.getStart().after(entry.getEnd())) {
			throw new TimesheetFault(Strings.ENTRY_IS_FINISHED_BEFORE_IT_BEGINS);
		}

		// create new entry
		TimesheetWorkEntry localEntry = new TimesheetWorkEntry();
		localEntry.setStart(entry.getStart());
		localEntry.setEnd(entry.getEnd());
		localEntry.setAssociatedProject(localProject);
		localSession.getUser().getEntries().add(localEntry);
		
		em.persist(localEntry);
		em.persist(localProject);

		return true;
	}

	@Override
	public boolean addUserToWork(TimesheetUserSession session, TimesheetWorkEntry entry, TimesheetUser user)
			throws TimesheetFault {
		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		// get local project
		TimesheetProject localProject = em.find(TimesheetProject.class, entry.getAssociatedProject().getId());

		if (localProject == null) {
			throw new TimesheetFault(Strings.NO_SUCH_PROJECT);
		}

		if (!localProject.getAssociatedWorkers().contains(localSession.getUser())) {
			throw new TimesheetFault(Strings.USER_AND_PROJECT_ARE_NOT_ASSOCIATED);
		}

		TimesheetWorkEntry localEntry = em.find(TimesheetWorkEntry.class, entry.getId());

		if (localEntry == null) {
			throw new TimesheetFault(Strings.NO_SUCH_ENTRY);
		}

		TimesheetUser localUser = em.find(TimesheetUser.class, user.getId());

		if (localUser == null) {
			throw new TimesheetFault(Strings.NO_SUCH_USER);
		}

		if (!localProject.getAssociatedWorkers().contains(localUser)) {
			throw new TimesheetFault(Strings.ADDED_USER_AND_PROJECT_ARE_NOT_ASSOCIATED);
		}

		localUser.getEntries().add(localEntry);

		em.persist(localUser);
		em.persist(localEntry);

		return false;
	}

	@Override
	public Collection<TimesheetUser> getUsersForProject(TimesheetUserSession session, TimesheetProject project)
			throws TimesheetFault {

		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		TimesheetProject localProject = em.find(TimesheetProject.class, project.getId());

		if (localProject.getAssociatedWorkers().contains(localSession.getUser())) {
			return localProject.getAssociatedWorkers();
		} else {
			throw new TimesheetFault(Strings.USER_AND_PROJECT_ARE_NOT_ASSOCIATED);
		}
	}

	@Override
	public Collection<TimesheetWorkEntry> getAssociatedEntries(TimesheetUserSession session) throws TimesheetFault {
		TimesheetUserSession localSession = authenticator.checkSessionCredentials(session, REQUIRED_ROLE);

		return localSession.getUser().getEntries();
	}
}
