package timesheet.core.impl;

public class Strings {
	public static final String LOGIN_FAILED = "Sikertelen bejelentkezés. ";
	public static final String AUTHENTICATION_FAILED = "Érvénytelen belépési adatok. ";
	public static final String ADDED_USER_AND_PROJECT_ARE_NOT_ASSOCIATED = "A hozzáadott felhasználól nincs hozzárendelve a projekthez. ";
	public static final String NO_SUCH_ROLE = "Nincs ilyen szerep. ";
	public static final String NO_SUCH_USER = "Nincs ilyen felhasználó. ";
	public static final String NO_SUCH_ENTRY = "Nincs ilyen bejegyzés. ";
	public static final String NO_SUCH_SESSION = "Nincs ilyen munkamenet. Jelentkezz be újra. ";
	public static final String NO_SUCH_PROJECT = "Nincs ilyen projekt. ";
	public static final String ENTRY_IS_FINISHED_BEFORE_IT_BEGINS = "A bejegyzés érvénytelen, mivel elöbb ér véget mint elkezdödik. ";
	public static final String USER_AND_PROJECT_ARE_NOT_ASSOCIATED = "Nem vagy hozzárendelve ehez a projekthez. ";
	public static final String INVALID_SESSION_TYPE = "Érvénytelen munkamenet típus. ";
	public static final String MISSING_ROLE = "Nem rendelkezel a megfelelő szerepkörrel. ";
	public static final String INVALID_PARAMETERS_USERNAME_TAKEN = "A felhasználó név már foglalt. ";
	public static final String INVALID_PARAMETERS_ACTION_CANNOT_BE_TAKEN = "Érvénytelen paraméterek. A művelet nem hajtható végre. ";
}
