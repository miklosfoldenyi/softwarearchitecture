package timesheet.core;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.jws.WebParam;
import javax.jws.WebService;

import timesheet.exceptions.TimesheetFault;
import timesheet.model.TimesheetProject;
import timesheet.model.TimesheetUser;
import timesheet.model.TimesheetUserSession;
import timesheet.model.TimesheetWorkEntry;

@WebService(targetNamespace = "http://timesheet.bme.hu/wsdl")
public interface ITimesheetManagerInterface {
	/**
	 * @throws RemoteException
	 *             Authenticates the user. This is a prerequisite to all
	 *             administrative operations via the manager interface.
	 * 
	 * @param username
	 * @param password
	 * @return a session id to be used in future calls.
	 * @throws
	 */
	TimesheetUserSession login(@WebParam(name = "user") TimesheetUser user, @WebParam(name = "password") String password)
			throws TimesheetFault;

	boolean logout(@WebParam(name = "session") TimesheetUserSession session) throws TimesheetFault;

	boolean createUser(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user, @WebParam(name = "password") String initialPassword)
			throws TimesheetFault;

	boolean deleteUser(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user) throws TimesheetFault;

	boolean grantClientRole(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user) throws TimesheetFault;

	boolean revokeClientRole(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user) throws TimesheetFault;

	boolean grantManageRole(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user) throws TimesheetFault;

	boolean revokeManageRole(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "user") TimesheetUser user) throws TimesheetFault;

	Collection<TimesheetUser> listAllUsers(@WebParam(name = "session") TimesheetUserSession session)
			throws TimesheetFault;

	boolean createProject(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "project") TimesheetProject project) throws TimesheetFault;

	boolean deleteProject(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "project") TimesheetProject project) throws TimesheetFault;

	Collection<TimesheetProject> listAllProjects(@WebParam(name = "session") TimesheetUserSession session)
			throws TimesheetFault;

	boolean addUserToProject(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "project") TimesheetProject project, @WebParam(name = "user") TimesheetUser user)
			throws TimesheetFault;

	boolean removeUserFromProject(@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "project") TimesheetProject project, @WebParam(name = "user") TimesheetUser user)
			throws TimesheetFault;

	Collection<TimesheetWorkEntry> listEntriesForUsersAndProjects(
			@WebParam(name = "session") TimesheetUserSession session,
			@WebParam(name = "users") Collection<TimesheetUser> users,
			@WebParam(name = "projects") Collection<TimesheetProject> projects) throws TimesheetFault;
}
