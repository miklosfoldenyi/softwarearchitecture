package timesheet.exceptions;

import javax.xml.ws.WebFault;

@WebFault
public class TimesheetFault extends Exception {

	private static final long serialVersionUID = 1L;

	public TimesheetFault(String message, BackendException cause) {
		super(cause.getMessage());
	}

	public TimesheetFault(String message) {
		super(message);
	}
}
