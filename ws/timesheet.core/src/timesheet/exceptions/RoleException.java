package timesheet.exceptions;

public class RoleException extends BackendException {

	private static final long serialVersionUID = 1L;

	public RoleException() {
		super("The user is missing a ruquired role.");
	}

	public RoleException(String message) {
		super(message);
	}
}
