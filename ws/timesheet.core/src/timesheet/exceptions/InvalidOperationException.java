package timesheet.exceptions;

public class InvalidOperationException extends BackendException {

	private static final long serialVersionUID = 1L;

	public InvalidOperationException() {
		super("The requested operation can not be executed.");
	}

	public InvalidOperationException(String message) {
		super(message);
	}
}
