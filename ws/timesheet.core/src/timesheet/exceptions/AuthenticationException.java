package timesheet.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException
public class AuthenticationException extends BackendException {

	private static final long serialVersionUID = 1L;

	public AuthenticationException() {
		super("The user credentials were incorrect.");
	}

	public AuthenticationException(String msg) {
		super(msg);
	}
}
