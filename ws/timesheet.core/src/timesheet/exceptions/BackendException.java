package timesheet.exceptions;

public class BackendException extends Exception {
	private static final long serialVersionUID = 1L;

	public BackendException() {
		super("The user credentials were incorrect.");
	}

	public BackendException(String msg) {
		super(msg);
	}
}
