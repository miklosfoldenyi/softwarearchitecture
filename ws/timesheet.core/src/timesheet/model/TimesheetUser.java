package timesheet.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class TimesheetUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;

	@Column(unique = true)
	private String username;

	private String passwordHash;

	@OneToMany
	private Collection<TimesheetUserRole> roles = new ArrayList<>();

	@ManyToMany
	private Collection<TimesheetWorkEntry> entries = new ArrayList<>();

	@ManyToMany
	private Collection<TimesheetProject> associatedProjects = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Collection<TimesheetUserRole> getRoles() {
		return roles;
	}

	public void setRoles(Collection<TimesheetUserRole> roles) {
		this.roles = roles;
	}

	public Collection<TimesheetWorkEntry> getEntries() {
		return entries;
	}

	public void setEntries(Collection<TimesheetWorkEntry> entries) {
		this.entries = entries;
	}

	public Collection<TimesheetProject> getAssociatedProjects() {
		return associatedProjects;
	}

	public void setAssociatedProjects(Collection<TimesheetProject> associatedProjects) {
		this.associatedProjects = associatedProjects;
	}
}
