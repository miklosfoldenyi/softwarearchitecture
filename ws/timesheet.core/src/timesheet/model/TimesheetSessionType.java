package timesheet.model;

public enum TimesheetSessionType {
	MANAGEMENT_SESSION, CLIENT_SESSION
}
