package timesheet.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class TimesheetWorkEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private TimesheetProject associatedProject;

	@ManyToMany(mappedBy = "entries")
	private Collection<TimesheetUser> attendees  = new ArrayList<>();

	private Date start;

	private Date end;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TimesheetProject getAssociatedProject() {
		return associatedProject;
	}

	public void setAssociatedProject(TimesheetProject associatedProject) {
		this.associatedProject = associatedProject;
	}

	public Collection<TimesheetUser> getAttendees() {
		return attendees;
	}

	public void setAttendees(Collection<TimesheetUser> attendees) {
		this.attendees = attendees;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
