package timesheet.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class TimesheetProject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(unique=true)
	private String name;

	@ManyToMany(mappedBy = "associatedProjects")
	private Collection<TimesheetUser> associatedWorkers  = new ArrayList<>();

	@OneToMany(mappedBy = "associatedProject")
	private Collection<TimesheetWorkEntry> associatedEntries  = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<TimesheetUser> getAssociatedWorkers() {
		return associatedWorkers;
	}

	public void setAssociatedWorkers(Collection<TimesheetUser> associatedWorkers) {
		this.associatedWorkers = associatedWorkers;
	}

	public Collection<TimesheetWorkEntry> getAssociatedEntries() {
		return associatedEntries;
	}

	public void setAssociatedEntries(Collection<TimesheetWorkEntry> associatedEntries) {
		this.associatedEntries = associatedEntries;
	}

}
