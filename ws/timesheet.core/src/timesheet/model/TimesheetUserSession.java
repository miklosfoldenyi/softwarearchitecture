package timesheet.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class TimesheetUserSession {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String sessionHash;

	@OneToOne
	private TimesheetUser user;

	private Date startTime;

	private Date endTime;
	
	private String note;

	private TimesheetSessionType type;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TimesheetUser getUser() {
		return user;
	}

	public void setUser(TimesheetUser user) {
		this.user = user;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public TimesheetSessionType getType() {
		return type;
	}

	public void setType(TimesheetSessionType type) {
		this.type = type;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSessionHash() {
		return sessionHash;
	}

	public void setSessionHash(String sessionHash) {
		this.sessionHash = sessionHash;
	}
	
	public TimesheetUserSession safeClone() {
		TimesheetUserSession session = new TimesheetUserSession();
		session.setStartTime(getStartTime());
		session.setId(getId());
		session.setType(getType());
		session.setSessionHash(getSessionHash());
		return session;
	}

}
